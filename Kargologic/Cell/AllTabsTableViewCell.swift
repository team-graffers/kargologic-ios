//
//  AllTabsTableViewCell.swift
//  Kargologic
//
//  Created by mac on 28/04/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class AllTabsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shortNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var jobNoLabel: UILabel!
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var deliveryLocationLabel: UILabel!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var deliveryDateLabel: UILabel!
    @IBOutlet weak var pickUpTimeLabel: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    
     var CallButtonClick:(() -> ())?
    var PODButtonClick:(() -> ())?
    var ViewMoreButtonClick:(() -> ())?
    
    @IBAction func callButtonTapped(_ sender: UIButton) {
        self.CallButtonClick?()
    }
    @IBAction func podButtonTapped(_ sender: UIButton) {
        self.PODButtonClick?()
    }
    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
        self.ViewMoreButtonClick?()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
