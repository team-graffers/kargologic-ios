//
//  ViewMoreTableViewCell.swift
//  Kargologic
//
//  Created by sachin jain on 07/06/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class ViewMoreTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ActualDateTimeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var ETAImage: UIImageView!
    @IBOutlet weak var processIcon: UIImageView!
    @IBOutlet weak var markAsCompleteButton: UIButton!
    @IBOutlet weak var rightTickImage: UIImageView!
    @IBOutlet weak var viewPodButton: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var btnAssets: UIButton!
    
    @IBOutlet weak var etaLbl: UILabel!
    // Attachment Outlets
    @IBOutlet weak var attachmentNameLabel: UILabel!
    var urlString = String()
    
    // Commens Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var commentNameLabel: UILabel!
    
    var CapturePODButtonClick:(() -> ())?
    var ShowPODButtonClick:(() -> ())?
    var MarkAsCompleteButtonClick:(() -> ())?
    var EnRounteButtonClick:(() -> ())?
    var downloadFileButtonClick:(() -> ())?
   var assetView:(() -> ())?
    
    var openFile:((_ path: String)->())?
    
    @IBAction func capturePodButtonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 10: // En-route event call
            self.EnRounteButtonClick?()
        case 11: // cmark as complete event call
            self.MarkAsCompleteButtonClick?()
        case 12: // capture pod event call
            self.CapturePODButtonClick?()
        default:
            break
        }
        
    }
    @IBAction func assetsAction(_ sender: Any) {
        self.assetView?()
    }
    @IBAction func showPodButtonTapped(_ sender: UIButton) {
          self.ShowPODButtonClick?()
    }
    @IBAction func downloadFileButtonTapped(_ sender: UIButton) {
//        self.downloadFileButtonClick?()
        downloadFiles(urlString : urlString.replacingOccurrences(of: " ", with: "%20"))
    
    }

    
    func downloadFiles(urlString : String) {
        
        var rootViewController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (rootViewController.presentedViewController != nil) {
            rootViewController = rootViewController.presentedViewController!
        }
        
      
        let url = URL(string: urlString)
        let fileName = String((url!.lastPathComponent)) as NSString
        // Create destination URL
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as! URL
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: urlString)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath : String = path[0]
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/\(fileName)"))
        
        if FileManager.default.fileExists(atPath: destinationURLForFile.path) {
            print("File Exists in app data")
            showDownloadFileActionSheet(pathUrl:  destinationURLForFile)
            
        } else {
            
            print( "Download file" )
            startDownloading(requestUrl: url!)
        }
        
        //        let alert = UIAlertController(title: "Downloading...", message: "", preferredStyle: .alert)
        //        rootViewController.present(alert, animated: true, completion: nil)
        //        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
        //            alert.dismiss(animated: true, completion: nil)
        //            if let tempLocalUrl = tempLocalUrl, error == nil {
        //
        //                // Success
        //                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
        //
        //                    print("Successfully downloaded. Status code: \(statusCode)")
        //                    let fileManager = FileManager()
        //                    do {
        //                        try fileManager.moveItem(at: tempLocalUrl, to: destinationFileUrl)
        //                        // show file
        //                        //                showFileWithPath(path: destinationURLForFile.path)
        //                    } catch {
        //                        print("An error occurred while moving file to destination url")
        //                    }
        //                }
        ////                do {
        ////                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
        ////                    do {
        ////                        //Show UIActivityViewController to save the downloaded file
        ////                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
        ////                        for indexx in 0..<contents.count {
        ////                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
        ////
        ////                                let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
        ////                                rootViewController.present(activityViewController, animated: true, completion: nil)
        ////                            }
        ////                        }
        ////                    }
        ////                    catch (let err) {
        ////                        print("error: \(err)")
        ////                    }
        ////                } catch (let writeError) {
        ////                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
        ////                    let alert = AppTheme.showAlert(writeError.localizedDescription, message: "", cancelButtonTitle: "OK")
        ////                    rootViewController.present(alert, animated: true, completion: nil)
        ////                }
        //            } else {
        //                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
        //            }
        //        }
        //        task.resume()
    }
    
    func startDownloading(requestUrl: URL) {
        
        var rootViewController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (rootViewController.presentedViewController != nil) {
            rootViewController = rootViewController.presentedViewController!
        }
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:requestUrl)
        //        let alert = AppTheme.showAlert("Downloading...", message: "", cancelButtonTitle: "")
        let alert = UIAlertController(title: "Downloading...", message: "", preferredStyle: .alert)
        rootViewController.present(alert, animated: true, completion: nil)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            alert.dismiss(animated: true, completion: nil)
            if  error == nil {
                
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    let fileManager = FileManager()
                    let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
                    let documentDirectoryPath : String = path[0]
                    let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/\(requestUrl.path)"))
                    let fileName = String((requestUrl.lastPathComponent)) as NSString
                    let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as! URL
                    let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
                    
                    if FileManager.default.fileExists(atPath: destinationURLForFile.path) {
                        do {
                            try fileManager.removeItem(atPath:  destinationURLForFile.path)
                            try fileManager.moveItem(at: tempLocalUrl!, to: destinationFileUrl)
                            // show file
                            self.showDownloadFileActionSheet(pathUrl: destinationURLForFile)
                            print(destinationFileUrl.path)
                            DispatchQueue.main.async {
                                self.openFile?(destinationURLForFile.path)
                            }
                        } catch {
                            print("An error occurred while moving file to destination url")
                        }
                    }else {
                        do {
                            try fileManager.moveItem(at: tempLocalUrl!, to: destinationFileUrl)
                            // show file
                            self.showDownloadFileActionSheet(pathUrl: destinationURLForFile)
                            
                        } catch {
                            print("An error occurred while moving file to destination url")
                        }
                    }
                    print("Successfully downloaded. Status code: \(statusCode)")
                }else {
                    print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
                }
            }
        }
        task.resume()
    }
    
    
    
    func showDownloadFileActionSheet (pathUrl: URL ) {
        
        print(pathUrl.path)
        
        //showFileWithPath(path: pathUrl.path)
        DispatchQueue.main.async {
            self.openFile?(pathUrl.path)

        }
        
        
//
//
//        var rootViewController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
//        while (rootViewController.presentedViewController != nil) {
//            rootViewController = rootViewController.presentedViewController!
//        }
//
//        let actionSheet = UIAlertController(title: "File already exists", message: nil, preferredStyle: .actionSheet)
//
//        let overwriteFile = UIAlertAction(title: NSLocalizedString("Overwrite File", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
//            actionSheet.dismiss(animated: true, completion: nil)
//
//            //Redownload File From path
//
//            self.startDownloading(requestUrl: pathUrl)
//
//        })
//        let cancel = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .cancel , handler: nil)
//
//        actionSheet.addAction(overwriteFile)
//        actionSheet.addAction(cancel)
//        rootViewController.present(actionSheet, animated: true, completion: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
