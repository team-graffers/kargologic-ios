//
//  HomeDataModel.swift
//  Kargologic
//
//  Created by sachin jain on 05/06/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit


class HomeDataModel: NSObject {
    
//    {
    //    "extra_detail": {},
//    "pickup": [ ],
//    "delivery": [ ]
    
//}
    
    var extraDetailsDictionary = ExtraDetails()
    var pickUpArray = [PickUp]()
    var deliveryArray = [Delivery]()
    var etaPickUp = ErrorMesssage.etaErrMsg
    var etaDelivery = ErrorMesssage.etaErrMsg
    
    var pickUpArrayAndDeliveryArray = [PickUpAndDelivery]()
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        if let extraDetailsDictionary = response["extra_detail"] as? [String:Any] {
            
                self.extraDetailsDictionary = ExtraDetails(withDictionary: extraDetailsDictionary)
            
        }

        if let pickUpArray = response["pickup"] as? [[String:Any]] {
            
            for item in pickUpArray {
                self.pickUpArray.append(PickUp.init(withDictionary: item))
                self.pickUpArrayAndDeliveryArray.append(PickUpAndDelivery.init(withDictionary: item))
            }
        }

        if let deliveryArray = response["delivery"] as? [[String:Any]] {
            
            for item in deliveryArray {
                self.deliveryArray.append(Delivery.init(withDictionary: item))
                self.pickUpArrayAndDeliveryArray.append(PickUpAndDelivery.init(withDictionary: item))
            }
        }
        
    }
}
class ExtraDetails: NSObject {

    //    "order_id": 62,
    //    "load_carried": "pallets",
    //    "load_type": "Grocery",
    //    "load_weight": "360kg",
    //    "length_load_dimen": "1",
    //    "width_load_dimen": "1",
    //    "height_load_dimen": "1",
    //    "measure_unit": "meter",
    //    "units": "60",
    //    "hazardous": false,
    //    "additional_note": "bring pallets back to the depot",
    //    "job_no": "101",
    //    "customer_company_id": 19,
    //    "customer_company_name": "NOT REQUIRED",
    //    "customer_company_number": "1234567890",
    //    "status": "pending"
    //    "order_enroute": "2019-06-04T11:07:59Z",
    //    "order_final_deliver": null
    
    var order_id = 0
    var load_carried = ""
    var load_type = ""
    var load_weight = ""
    var length_load_dimen = ""
    var width_load_dimen = ""
    var height_load_dimen = ""
    var measure_unit = ""
    var units = ""
    var hazardous = false
    var additional_note = ""
    var job_no = ""
    var customer_company_id = 0
    var customer_company_name = ""
    var customer_company_number = ""
    var status = ""
    var order_enroute = ""
    var order_final_deliver = ""
    var carrier_number = ""
    
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        self.order_id = response["order_id"] as? Int ?? 0
        self.load_carried = response["load_carried"] as? String ?? ""
        self.load_type = response["load_type"] as? String ?? ""
        self.load_weight = response["load_weight"] as? String ?? ""
        self.length_load_dimen = response["length_load_dimen"] as? String ?? ""
        self.width_load_dimen = response["width_load_dimen"] as? String ?? ""
        self.height_load_dimen = response["height_load_dimen"] as? String ?? ""
        self.measure_unit = response["measure_unit"] as? String ?? ""
        self.units = response["units"] as? String ?? ""
        self.hazardous = response["hazardous"] as? Bool ?? false
        self.additional_note = response["additional_note"] as? String ?? ""
        self.job_no = response["job_no"] as? String ?? ""
        self.customer_company_id = response["customer_company_id"] as? Int ?? 0
        self.customer_company_name = response["customer_company_name"] as? String ?? ""
        self.customer_company_number = String(describing: response["customer_company_number"] as AnyObject)
        self.status = response["status"] as? String ?? ""
        
//        print(response["status"] as? String ?? "")
//        
//        switch response["status"] as? String ?? "" {
//        case "in process":
//            self.status = "In-Progress"
//        case "delivered":
//            self.status = "Completed"
//        default:
//            self.status = "Pending"
//        }
        
        
        
        self.order_enroute = response["order_enroute"] as? String ?? ""
        self.order_final_deliver = response["order_final_deliver"] as? String ?? ""
        self.carrier_number = response["carrier_company_number"] as? String ?? ""
        //carrier_company_number
    }
    
}


struct Asset{
    var assetId = ""
    var assetType = ""
    var regNum = ""
}

class PickUp: NSObject {
    
    //    {
    //    "data_of": "pickup",
    //    "id": 114,
    //    "pickup_date_time": "2019-06-04T04:00:00Z",
    //    "pickup_date_time_to": "2019-06-04T04:30:00Z",
    //    "actual_pickup_date_time": null,
    //    "address": "Australia, VIC, Cranbourne East",
    //    "longitude": "-38.09504",
    //    "latitude": "145.29339",
    //    "assigned_quantity": "30"
    //    },
    
    var data_of = ""
    var id = 0
    var pickup_date_time = ""
    var pickup_date_time_to = ""
    var actual_pickup_date_time = ""
    var address = ""
    var longitude = ""
    var latitude = ""
    var assigned_quantity = ""
    
    // addtional key
    
    var timesTamp = Int64()
    
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        self.data_of = response["data_of"] as? String ?? ""
        self.id = response["id"] as? Int ?? 0
        self.pickup_date_time = response["pickup_date_time"] as? String ?? ""
        self.timesTamp = AppTheme.dateFromString(pickup_date_time).millisecondsSince1970 
        self.pickup_date_time_to = response["pickup_date_time_to"] as? String ?? ""
        self.actual_pickup_date_time = response["actual_pickup_date_time"] as? String ?? ""
        self.address = response["address"] as? String ?? ""
        self.longitude = response["longitude"] as? String ?? ""
        self.latitude = response["latitude"] as? String ?? ""
        self.assigned_quantity = response["assigned_quantity"] as? String ?? ""
    }
}
class Delivery: NSObject {
    //    {
    //    "data_of": "delivery",
    //    "id": 116,
    //    "delivery_date_time": "2019-06-04T07:00:00Z",
    //    "delivery_date_time_to": "2019-06-04T07:30:00Z",
    //    "actual_delivery_date_time": null,
    //    "address": "8 Liverpool Court, Narre Warren South",
    //    "longitude": "-38.06383",
    //    "latitude": "145.29265",
    //    "assigned_quantity": "60"
    // "pod_image": "https://s3-ap-southeast-2.amazonaws.com/kargologic/media/pod/90-SACH-sandeep-176.jpg"
    //    }
    
    var data_of = ""
    var id = 0
    var delivery_date_time = ""
    var delivery_date_time_to = ""
    var actual_delivery_date_time = ""
    var address = ""
    var longitude = ""
    var latitude = ""
    var assigned_quantity = ""
    var pod_image = ""
    
    // addtional key
    var timesTamp = Int64()
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        self.data_of = response["data_of"] as? String ?? ""
        self.id = response["id"] as? Int ?? 0
        self.delivery_date_time = response["delivery_date_time"] as? String ?? ""
        self.timesTamp = AppTheme.dateFromString(delivery_date_time).millisecondsSince1970
        self.delivery_date_time_to = response["delivery_date_time_to"] as? String ?? ""
        self.actual_delivery_date_time = response["actual_delivery_date_time"] as? String ?? ""
        self.address = response["address"] as? String ?? ""
        self.longitude = response["longitude"] as? String ?? ""
        self.latitude = response["latitude"] as? String ?? ""
        self.assigned_quantity = response["assigned_quantity"] as? String ?? ""
        self.pod_image = response["pod_image"] as? String ?? ""
    }
}
class PickUpAndDelivery: NSObject {
    
    //    {
    //    "data_of": "pickup",
    //    "id": 114,
    //    "pickup_date_time": "2019-06-04T04:00:00Z",
    //    "pickup_date_time_to": "2019-06-04T04:30:00Z",
    //    "actual_pickup_date_time": null,
    //    "address": "Australia, VIC, Cranbourne East",
    //    "longitude": "-38.09504",
    //    "latitude": "145.29339",
    //    "assigned_quantity": "30"
    //    },
    
    var etaPickUp = ErrorMesssage.etaErrMsg
    var etaDelivery = ErrorMesssage.etaErrMsg
    
    
    var data_of = ""
    var id = 0
    var pickup_date_time = ""
    var pickup_date_time_to = ""
    var actual_pickup_date_time = ""
    
    var delivery_date_time = ""
    var delivery_date_time_to = ""
    var actual_delivery_date_time = ""
    var pod_image = ""
    var senderName = ""
    var deliveryName = ""
    
    var address = ""
    var longitude = ""
    var latitude = ""
    var assigned_quantity = ""
    var assetList = [Asset]()
    // addtional key
    
    var timesTamp = Int64()
    
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        print(response)
        
        self.data_of = response["data_of"] as? String ?? ""
        self.id = response["id"] as? Int ?? 0
        self.pickup_date_time = response["pickup_date_time"] as? String ?? ""
        self.pickup_date_time_to = response["pickup_date_time_to"] as? String ?? ""
        self.actual_pickup_date_time = response["actual_pickup_date_time"] as? String ?? ""
        
        self.delivery_date_time = response["delivery_date_time"] as? String ?? ""
        self.delivery_date_time_to = response["delivery_date_time_to"] as? String ?? ""
        self.actual_delivery_date_time = response["actual_delivery_date_time"] as? String ?? ""
        self.pod_image = response["pod_image"] as? String ?? ""
        
        self.address = response["address"] as? String ?? ""
        self.longitude = response["longitude"] as? String ?? ""
        self.latitude = response["latitude"] as? String ?? ""
        self.assigned_quantity = response["assigned_quantity"] as? String ?? ""
        
        
        
        
        
        if let assetList = response["asset_list"] as? [[String: Any]]{
            for item in assetList{
                self.assetList.append(Asset(assetId: item["asset_id"] as? String ?? "",
                                            assetType: item["asset_type"] as? String ?? "",
                                            regNum: item["registeration_no"] as? String ?? ""
                                            ))
            }
        }
        
        
        if self.data_of == "delivery"{
            
            if let sender = (response["receiver"] as? [String: Any]){
                if let name  = sender["name"] as? String {
                    self.senderName = name ?? ""
                }
            }
            
            
         self.timesTamp = AppTheme.dateFromString(delivery_date_time).millisecondsSince1970
        }else if self.data_of == "pickup" {
            
            if let sender = (response["sender"] as? [String: Any]){
                if let name  = sender["name"] as? String {
                    self.senderName = name ?? ""
                }
            }
          self.timesTamp = AppTheme.dateFromString(pickup_date_time).millisecondsSince1970
        }
    }
}
