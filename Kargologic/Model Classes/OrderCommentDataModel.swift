//
//  OrderCommentDataModel.swift
//  Kargologic
//
//  Created by sachin jain on 08/06/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class OrderCommentDataModel: NSObject {
    
    //    {
    //    "id": 31,
    //    "comment": "THIS ORDER IS FOR SACHIN",
    //    "Created_by_CustomerID": null,
    //    "Created_by_CarrierID": {
    //    "id": 1,
    //    "name": "Kritesh vani"
    //    },
    //    "Created_by_DriverID": null,
    //    "created_DT": "2019-06-07T09:24:43.139219Z",
    //    "updated_DT": "2019-06-07T09:24:43.139243Z"
    //    }
    
    var id = 0
    var comment = ""
    var Created_by_CustomerID = CreatedByCustomer()
    var Created_by_CarrierID = CreatedByCarrier()
    var Created_by_DriverID = CreatedByDriver()
    var created_DT = ""
    var updated_DT = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        self.id = response["id"] as? Int ?? 0
        self.comment = response["comment"] as? String ?? ""
        
        if let CreatedByCustomerDict = response["Created_by_CustomerID"] as? [String:Any] {
            
            self.Created_by_CustomerID = CreatedByCustomer(withDictionary: CreatedByCustomerDict)
            
        }
        if let CreatedByDriverDict = response["Created_by_DriverID"] as? [String:Any] {
            
            self.Created_by_DriverID = CreatedByDriver(withDictionary: CreatedByDriverDict)
            
        }
        if let CreatedByCarrierDict = response["Created_by_CarrierID"] as? [String:Any] {
            
            self.Created_by_CarrierID = CreatedByCarrier(withDictionary: CreatedByCarrierDict)
            
        }
        self.created_DT = response["created_DT"] as? String ?? ""
        self.updated_DT = response["updated_DT"] as? String ?? ""
    }
}
class CreatedByCustomer: NSObject {
    //    "id": 1,
    //    "name": "sandeep"
    
    var id = 0
    var name = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        self.id = response["id"] as? Int ?? 0
        self.name = response["name"] as? String ?? ""
        
    }
}
class CreatedByCarrier: NSObject {
    //    "id": 1,
    //    "name": "sandeep"
    
    var id = 0
    var name = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        self.id = response["id"] as? Int ?? 0
        self.name = response["name"] as? String ?? ""
    }
}
class CreatedByDriver: NSObject {
    //    "id": 1,
    //    "name": "sandeep"
    
    var id = 0
    var name = ""
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        self.id = response["id"] as? Int ?? 0
        self.name = response["name"] as? String ?? ""
    }
}

