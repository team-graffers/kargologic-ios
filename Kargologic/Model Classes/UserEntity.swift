//
//  UserEntity.swift
//  Kargologic
//
//  Created by mac on 27/04/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class UserEntity: NSObject  {
    
//    {
//    "token": "e798c808181aaa773430fec558e9b135b1b5d1d9",
//    "name": "Sachin Jain",
//    "phone": "9856321452",
//    "email": "sj11021994@gmail.com",
//    "trackingId": "HERE-e2fb2c7c-2797-4570-bba1-a6d5d70b5c9c",
//    "deviceId": "82ce62ab-b99e-4351-97c1-da2064aa70bb",
//    "deviceSecret": "mX3tAe8x78IgsUhkqo1aQyeit4l5NxFhCYOYGyO9btg",
//    "image": "https://s3-ap-southeast-2.amazonaws.com/kargologic/media/driver/profile-pic.png",
//    "license_num": "JTFYFFI7969O",
//    "categ_name": "8 Axle B-double",
//    "cate_id": 55,
//    "truck_reg_num": "MP09SA0420",
//    "carr_id": 1,
//    "carr_com_id": 1,
//    "message": "LoggedIn Successfully"
//    }

  
    var token = ""
    var name = ""
    var phone = ""
    var email = ""
    var license_num = ""
    var categ_name = ""
    var cate_id = 0
    var truck_reg_num = ""
    var carr_id = 0
    var carr_com_id = 0
    var message = ""
    var image = ""
    var trackingId = ""
    var deviceId = ""
    var deviceSecret = ""
   
//    "cate_id" = 5;
//    email = "patidars0143@gmail.com";
//    image = "https://s3-ap-southeast-2.amazonaws.com/kargologic/media/driver/5645785612";
//    "license_num" = bbh89O9;
//    name = Sandeep;
//    phone = 5645785612;
//    "truck_reg_num" = VGH6778;
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        
        
        print(response)
        
        if let token = response["token"] as? String
        {
            self.token = token
        }
        if let name = response["name"] as? String
        {
            self.name = name
        }
        if let phone = response["phone"] as? String
        {
            self.phone = phone
        }
        if let email = response["email"] as? String
        {
            self.email = email
        }
        if let carr_id = response["carr_id"] as? Int
        {
            self.carr_id = carr_id
        }
        if let carr_com_id = response["carr_com_id"] as? Int
        {
            self.carr_com_id = carr_com_id
        }
        if let message = response["message"] as? String
        {
            self.message = message
        }
        if let image = response["image"] as? String
        {
            self.image = image
        }
        if let license_num = response["license_num"] as? String
        {
            self.license_num = license_num
        }
        if let categ_name = response["categ_name"] as? String
        {
            self.categ_name = categ_name
        }
        if let cate_id = response["cate_id"] as? Int
        {
            self.cate_id = cate_id
        }
        if let truck_reg_num = response["truck_reg_num"] as? String
        {
            self.truck_reg_num = truck_reg_num
        }
        if let trackingId = response["trackingId"] as? String
        {
            self.trackingId = trackingId
        }
        if let deviceId = response["deviceId"] as? String
        {
            self.deviceId = deviceId
        }
        if let deviceSecret = response["deviceSecret"] as? String
        {
            self.deviceSecret = deviceSecret
        }
    }
    
//    // MARK: NSCoding
//    required convenience init(coder decoder: NSCoder) {
//        self.init()
//        //        self.stylemee_user_id = decoder.decodeObject(forKey: "stylemee_user_id") as? Int ?? 0
//        self.token = decoder.decodeObject(forKey: "token") as? String ?? ""
//        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
//        self.phone = decoder.decodeObject(forKey: "phone") as? String ?? ""
//        self.email = decoder.decodeObject(forKey: "email") as? String ?? ""
//        self.carr_id = decoder.decodeObject(forKey: "carr_id") as? Int ?? 0
//        self.carr_com_id = decoder.decodeObject(forKey: "carr_com_id") as? Int ?? 0
//        self.message = decoder.decodeObject(forKey: "message") as? String ?? ""
//        self.image = decoder.decodeObject(forKey: "image") as? String ?? ""
//        self.license_num = decoder.decodeObject(forKey: "license_num") as? String ?? ""
//        self.categ_name = decoder.decodeObject(forKey: "categ_name") as? String ?? ""
//        self.cate_id = decoder.decodeObject(forKey: "cate_id") as? Int ?? 0
//        self.truck_reg_num = decoder.decodeObject(forKey: "truck_reg_num") as? String ?? ""
//        
//    }
//    
//    public func encode(with aCoder: NSCoder) {
//        aCoder.encode(self.token, forKey: "token")
//        aCoder.encode(self.name, forKey: "name")
//        aCoder.encode(self.phone, forKey: "phone")
//        aCoder.encode(self.email, forKey: "email")
//        aCoder.encode(self.carr_id, forKey: "carr_id")
//        aCoder.encode(self.carr_com_id, forKey: "carr_com_id")
//        aCoder.encode(self.message, forKey: "message")
//        aCoder.encode(self.image, forKey: "image")
//        aCoder.encode(self.license_num, forKey: "license_num")
//        aCoder.encode(self.categ_name, forKey: "categ_name")
//        aCoder.encode(self.cate_id, forKey: "cate_id")
//        aCoder.encode(self.truck_reg_num, forKey: "truck_reg_num")
//    }
}

//extension UserEntity {
//
//    func fullname() -> String {
//        return stylemee_user_first_name + " " + stylemee_user_last_name
//    }
//
//}

