//
//  NotificationModel.swift
//  Kargologic
//
//  Created by shrikant upadhyay on 10/10/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import Foundation
class NotificationModel{
    let id: Int?
    let orderStatus, comments: String?
    let isRead: Bool?
    let dateTimeOfCreate: String?
    let isActive: Bool?
    let orderID, createdByCarrier: Int?
    let createdByCustomer, createdByDriver, createdForCarrier, createdForCustomer: String?
    let createdForDriver: Int?
    
    init(id: Int?, orderStatus: String?, comments: String?, isRead: Bool?, dateTimeOfCreate: String?, isActive: Bool?, orderID: Int?, createdByCarrier: Int?, createdByCustomer: String?, createdByDriver: String?, createdForCarrier: String?, createdForCustomer: String?, createdForDriver: Int?) {
        self.id = id
        self.orderStatus = orderStatus
        self.comments = comments
        self.isRead = isRead
        self.dateTimeOfCreate = dateTimeOfCreate
        self.isActive = isActive
        self.orderID = orderID
        self.createdByCarrier = createdByCarrier
        self.createdByCustomer = createdByCustomer
        self.createdByDriver = createdByDriver
        self.createdForCarrier = createdForCarrier
        self.createdForCustomer = createdForCustomer
        self.createdForDriver = createdForDriver
    }
}
