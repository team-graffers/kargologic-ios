//
//  VehicleTypeDataModel.swift
//  Kargologic
//
//  Created by sachin jain on 13/06/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class VehicleTypeDataModel: NSObject {

//    {
//    "id": 1,
//    "categ_name": "Truck",
//    "capacity": null,
//    "parent_id": null,
//    "is_last": false
//    }
    
    var id = 0
    var categ_name = ""
    var capacity = ""
    var parent_id = ""
    var is_last = false
    
    convenience init(withDictionary response:[String:Any]) {
        self.init()
        self.id = response["id"] as? Int ?? 0
        self.categ_name = response["categ_name"] as? String ?? ""
        self.capacity = response["capacity"] as? String ?? ""
        self.parent_id = response["parent_id"] as? String ?? ""
        self.is_last = response["is_last"] as? Bool ?? false
    }
    
}
