//
//  AttachmentDataModel.swift
//  Kargologic
//
//  Created by sachin jain on 08/06/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class AttachmentDataModel: NSObject {
//    "attachment_name": "david.jpg",
//    "physical_path": "https://s3-ap-southeast-2.amazonaws.com/kargologic/orderattachment/david.jpg"

    var attachment_name = ""
    var physical_path = ""

    convenience init(withDictionary response:[String:Any]) {
        self.init()
        self.attachment_name = response["attachment_name"] as? String ?? ""
        self.physical_path = response["physical_path"] as? String ?? ""
    }
}
