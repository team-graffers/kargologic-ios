

import UIKit


// MARK: - extension - Bundle
extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

// MARK: - extension - Application
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


// MARK: - extension - Date
extension Foundation.Date {
    struct Date1 {
        static let formatterCustom: DateFormatter = {
            let formatter = DateFormatter()
            return formatter
        }()
        static let formatterDate:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            return formatter
        }()
        static let StandardFormatterDate:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
        }()
        static let formatterTime:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            return formatter
        }()
        static let formatterWeekday:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE"
            return formatter
        }()
        static let formatterMonth:DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "LLL"
            return formatter
        }()
    }
    var date: String {
        return Date1.formatterDate.string(from: self)
    }
    var standardDate: String {
        return Date1.StandardFormatterDate.string(from: self)
    }
    var time: String {
        return Date1.formatterTime.string(from: self)
    }
    var weekdayName: String {
        return Date1.formatterWeekday.string(from: self)
    }
    
    var dayNumberOfWeek: Int {
        return calendar.component(.weekday, from: self)
    }
    
    var monthName: String {
        return  Date1.formatterMonth.string(from: self)
    }
    
    var year: Int {
        return components.year!
    }
    var components: DateComponents {
        return (calendar as NSCalendar).components([.year, .month, .weekday, .day, .hour, .minute, .second], from: self)
    }
    fileprivate var calendar: NSCalendar {
        return (NSCalendar.current as NSCalendar)
    }
    
    func stringFromFormat(_ format:String) -> String {
        Date1.formatterCustom.dateFormat = format
        return Date1.formatterCustom.string(from: self)
    }
    func formatted(format:String) -> String {
        Date1.formatterCustom.dateFormat = format
        return Date1.formatterCustom.string(from: self)
    }
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
    
    func currencyformat() -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter.string(from: NSNumber(value: self))!
    }
}
