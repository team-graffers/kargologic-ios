
//import UIKit
//
//class Constants: NSObject {
//
//}

import UIKit
import Foundation


//Development
let serverIP = "http://34.208.171.55/api/"

//Live
//let serverIP = "http://base3.engineerbabu.com:8282/qalame/api/"


let testingmode = true

enum WebServiceKey: String {
    case State_ID = "state_id"
    case Message = "message"
    case Status = "status"
    case username = "username"
    case password = "password"
    case accountStatus = "accountStatus"
    case nickName = "nickName"
    case firstName = "firstName"
    case lastName = "lastName"
    case email = "email"
    case city = "city"
    case state="state"
    case phone = "phone"
    case token = "token"
    case homeFeed = "homeFeed"
    case feed = "feed"
    case feedOptionHeaders   = "lm"
    case user_id = "user_id"
    case device_token = "device_token"
    case platform = "platform"
    case CreditBalance = "CreditBalance"
    case creditCap = "creditCap"
}

enum WebServiceStatus: Int {
    case error
    case success
    case invalidToken
    case invalidDevice
}


enum StoreyboardName : String {
    case start = "Start"
    case home = "Home"
    case search = "Search"
    case message = "Message"
    case noification = "Noification"
    case profile = "Profile"
}


//var popupType : PopupType = PopupType.init(rawValue: 1)!


// app messages
let PASSWORD_MIN_LENGTH = 5
let MSG_INVALID_EMAIL = "It seems Email address is not valid, Please enter valid Email address."
let MSG_INVALID_PASSWORD = "It seems that password length is less than \(PASSWORD_MIN_LENGTH) characters"
let NO_INTERNET_MSG = "Please Connect to internet."


//UserDefaults
let KUserData = "UserData"
let kDeviceToken = "DeviceToken"
let kSecretKey = "SecretKey"
let kIsUserLogin = "IsUserLogin"
let KFilterData = "FilterData"

//class Constants : NSObject {

// old URL ---> https://mybuddy24x7.com/buddyapp/
//URLConstants.swift
struct APPURL {
    //static let dev = "http://13.54.44.169:8000/"
    //http://52.62.137.8:8000/driv/driv_login/
    //http://52.62.137.8
    static let dev = "http://52.62.137.8:8000/driv/"
    static let live = "http://3.211.165.126:8000/driv/"
    
    //https://carriers.kargologic.com
    
    
//    private struct Domains {
//        static let Dev = "http://ec2-3-84-28-17.compute-1.amazonaws.com:8000/driv/"
//        static let Live = "https://buddyfortravel.com/buddyapp/"
//
//        static let UAT = "http://test-UAT.com"
//        static let Local = "192.145.1.1"
//        static let QA = "testAddress.qa.com"
//    }
//
////    private  struct Routes {
////        static let BuddyApi = "buddy/"
////        static let TravellerApi = "traveller/"
////    }
////
////    static let Domain = Domains.Dev
////    private  static let RouteBuddy = Routes.BuddyApi
////    private  static let RouteTraveller = Routes.TravellerApi
////
//    static let BaseURLBuddy = Domain + RouteBuddy
//    static let BaseURLTraveller = Domain + RouteTraveller
    
    //    static var FacebookLogin: String {
    //        return Domain  + "/auth/facebook"
    //    }
//    static let applicationUrl = "https://www.itunes.apple.com/app/id1234567890"
   static let applicationUrl = "https://itunes.apple.com/us/app/buddy-for-travel/id1453510897"
    
    struct webLink {
        
        static let termsAndCondition = "https://buddyfortravel.com/terms-condition"
        static let aboutUs = "https://buddyfortravel.com/#about"
        static let privacyPolicy = "https://buddyfortravel.com/privacy-policy"
        static let FAQ = "https://buddyfortravel.com/faq"
    }
}

struct GoogleAPIs {
    
  static func topAttractionAPI(city: String, apiKey: String) -> String {
    
          return "https://maps.googleapis.com/maps/api/place/textsearch/json?query=point+of+interest+in+\(city)&key=\(apiKey)"
    }
    static func photosOfPlacesAPI(width: String,height: String, photoKey: String, apiKey: String) -> String {
        
        return "https://maps.googleapis.com/maps/api/place/photo?maxheight=\(height)&maxwidth=\(width)&photoreference=\(photoKey)&key=\(apiKey)"
    }
    static func PlaceInfoAPI(place_id : String, apiKey: String) -> String {
        
       return "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(place_id)&fields=name,rating,formatted_phone_number,formatted_address,url,geometry,photo,rating,review,user_ratings_total,website&key=\(apiKey)"
    }
    static func staticMapAPI(lat: String,long: String, apiKey: String) -> String {
       
       return "http://maps.googleapis.com/maps/api/staticmap?center=\(lat),\(long)&zoom=17&size=400x400&maptype=roadmap&format=png&visual_refresh=true&markers=\(lat),\(long)&key=\(apiKey)"
    }
//    static let topAttractionAPI = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=point+of+interest+in+indore&key=AIzaSyBHzif2LuqZYAHLkSeEEQpy6OWfD2a7cug"
//    
//    static let photosOfPlacesAPI = "https://maps.googleapis.com/maps/api/place/photo?maxheight=400&photoreference=image&key=AIzaSyBHzif2LuqZYAHLkSeEEQpy6OWfD2a7cug"
}
//FontsConstants.swift
struct FontNames {
    static let RobotoName = "Rubik"
    struct Rubik {
        static let Medium = "Rubik-Medium"
        static let Regular = "Rubik-Regular"
    }
    
    //      static let Success = UIColor(red: 0.1303, green: 0.9915, blue: 0.0233, alpha: Alphas.Opaque)
    static let fontBold = UIFont(name: "Montserrat-Bold", size: 16)
}

//FontsConstants.swift
struct CellIdentifiers {
    static let inProgress = "InProgressCell"
    static let upcoming = "UpcomingCell"
    static let completed = "CompletedCell"
    static let viewMoreOrderStatus = "orderStatusCell"
    static let attachmentCell = "attachmentCell"
    static let commentCell = "commentCell"
}

//FOR ALL THE KEYS USED IN APP
//KeyConstants.swift
struct Key {
    
    static let DeviceType = "iOS"
    static let DeviceId = UIDevice.current.identifierForVendor?.uuidString
//    static let DeviceToken = AppTheme.getDeviceToken()
    static let AppVersion = Bundle.main.releaseVersionNumber
    static let BuildNumber = Bundle.main.buildVersionNumber
    static let Applozic_key = "16b11604b987a1136d0ef054532426a9a" //"stylemee281dac7c3db17f2e5dc503"
    
    
    struct Beacon{
        static let ONEXUUID = "xxxx-xxxx-xxxx-xxxx"
    }
    
    struct UserDefaults {
        static let k_App_Running_FirstTime = "userRunningAppFirstTime"
    }
    
    struct Headers {
        static let Authorization = "Authorization"
        static let ContentType = "Content-Type"
    }
    struct Google{
        static let clientId = "763488044224-l8nuv6ichf7jlmlg3i9ipk63dsrcqt3n.apps.googleusercontent.com"
        static let placesKey = "some key here" //for photos
        static let serverKey = "some key here"
        static let mapAPIKey = "AIzaSyDnllR7yc5SzhVzvnGabWl_fcsmXWQzmcc"
        static let APIKey = "AIzaSyAEu2rDhzlLsBHGKfq-e_ZSlbj_iov_zpk"
        //static let mapAPIKey = "AIzaSyALWUtsLWyY_Z8eiUyDxnH7IUelKYmx8B8"
    }
    struct Splunk{
        static let APIKey = "86623f84"
    }
    struct Stripe {
        
        static let publishableKey = "pk_test_8ddIJSY9q70gLCdv09nflycT"
//        static let publishableKey = "pk_live_bf5O6t08UafwCmGxpSh3Kbu9"
    }
    
    struct ErrorMessage{
        static let listNotFound = "ERROR_LIST_NOT_FOUND"
        static let validationError = "ERROR_VALIDATION"
    }
}


//  Device IPHONE
struct DeviceSize {
    
    static let kIphone_4s : Bool =  (UIScreen.main.bounds.size.height == 480)
    static let kIphone_5 : Bool =  (UIScreen.main.bounds.size.height == 568)
    static let kIphone_6 : Bool =  (UIScreen.main.bounds.size.height == 667)
    static let kIphone_6_Plus : Bool =  (UIScreen.main.bounds.size.height == 736)
}

struct ErrorMesssage {
    
    static let shortName: String  = "Name is too short"
    static let email: String  = "Enter Valid Email"
    static let password: String  = "Enter Password"
    static let newPassword: String  = "Enter New Password"
    static let passwordLength: String  = "Password length must be at least 6"
    static let confirmPassword: String  = "Enter confirm password"
    static let passwordMatch: String  = "Password does not matched"
   
    static let invalidSortCode: String   = "Sort code should be of 6 digits"
    static let sortMobile: String   = "Mobile Number is too short"
    static let messageEmpty: String   = "Please type something and try again"
    static let phoneNum: String   = "Mobile Number can't be empty"
    
    static let etaErrMsg: String = "ETA can't be calculated"

    
    
}
struct NotificationType {
    
    //Common text
    static let adminNotification = "15"
    
    
}
//}

//Subject : <Vineet > thinks you'd love Buddy too!
//
//
//Your friend <Vineet Rai> is so impressed with Buddy, they want to share the Buddy’s love with you. Come on board and enjoy new way of travel.
//
//Why Buddy ? Buddy is your local travel companion who you will address all your travel needs. Also Buddy offers free dashboard where you can save  all your travel information. Download now!!
//
//Google Play : https://bit.ly/2GHwYCX
//App Store : https://apple.co/2tyq1f3
//Website : https://buddyfortravel.com
//
//
//With Love,
//
//Team Buddy
//Email : Support@buddyfortravel.com

struct Texts {
    
    //Common text
    static let inviteSubject = "\(SharedPreference.getUserData().name) thinks you'd love Buddy too!"
    static let inviteText = "Your friend \(SharedPreference.getUserData().name) is so impressed with Buddy, they want to share the Buddy’s love with you. Come on board and enjoy new way of travel. \nWhy Buddy ?  Buddy is your local travel companion who you will address all your travel needs. Also Buddy offers free dashboard where you can save  all your travel information. Download now!! \n\nhttps://buddyfortravel.page.link/Invite \n\nWith Love,\nTeam Buddy \nEmail : Support@buddyfortravel.com"
    
}
extension Notification.Name {
    
    static let notificationBadge = Notification.Name( rawValue: "notificationBadge")
    static let notificationProfileScroll = Notification.Name( rawValue: "profileScroll")
    static let notificationSelectedService = Notification.Name( rawValue: "SelectedService")
    
    static let trackBarber = Notification.Name( rawValue: "trackBarber")
    
}


struct Helpers {
    
    static func validateResponse(_ statusCode: Int) -> Bool {
        if case 200...300 = statusCode {
            return true
        }
        return false
  }
}

struct JobStatus {
    
    static let inProgress = "in process"
    static let upcoming = "pending"
    static let completed = "delivered"
   
}
struct OrderType {
    
    static let pickUp = "pickup"
    static let delivery = "delivery"
    
}
