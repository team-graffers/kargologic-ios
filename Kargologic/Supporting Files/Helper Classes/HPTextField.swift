//
//  HPTextField.swift
//  TextileTrade
//
//  Created by mac on 28/08/17.
//  Copyright © 2017 mac. All rights reserved.
//

import UIKit

@IBDesignable
class HPTextField : UITextField {

    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
//        setup()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
//        setup()
    }
    
//
//    @IBInspectable var xContent : CGFloat = 0
//
//    override func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.insetBy(dx: xContent, dy: 0)
//    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    
    // properties..
    
    @IBInspectable var enableTitle : Bool = false

    @IBInspectable var placeHolderColor: UIColor = UIColor.white {
        
        didSet {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: placeHolderColor])
        }
    }
    
//    func setup() {
//        borderStyle = UITextBorderStyle.none
//        layer.borderWidth = CGFloat(borderWidth)
//        layer.borderColor = borderColor?.cgColor
//        placeHolderColor = UIColor.white
//
//        let bottomLineLayer = CALayer()
//        bottomLineLayer.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width , height: 1.0)
//        bottomLineLayer.backgroundColor = UIColor(red: 50/255, green: 50/255, blue: 25/255, alpha: 1).cgColor
//        layer.addSublayer(bottomLineLayer)
//        self.clipsToBounds = true
//
//
//    }
    
    // Disable Paste action from textfield
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        } else if action == #selector(UIResponderStandardEditActions.cut(_:)) {
            return false
        }
        
        return super.canPerformAction(action, withSender: sender)
    }

}
extension UITextField {
    
    func setImageOnleftView(img : String ,leftPading : Bool, isBottomBorder : Bool ) {
        
        if !img.isEmpty && leftPading {
            
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 50, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 18, y: 10, width: 18, height: 18))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
            
        } else if !img.isEmpty {
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 30, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 5, y: 10, width: 15, height: 15))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
            
        }  else if leftPading {
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 10, height: 35) )
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
        }
        
        if isBottomBorder {
            
            let bottomLayerEmail = CALayer()
            bottomLayerEmail.frame = CGRect(x: 0, y: self.frame.height-1, width: 1000, height: 0.6)
            bottomLayerEmail.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.layer.addSublayer(bottomLayerEmail)
            self.clipsToBounds = true
        }
    }
    
    func setImageOnRightView(img : String ,rightPadding : Bool, isBottomBorder : Bool ) {
       
        
        if !img.isEmpty && rightPadding {
          
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 35, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 10, y: 13, width: 13, height: 8))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.rightView = vwForImg
            self.rightViewMode = UITextField.ViewMode.always
            
        } else if  !img.isEmpty {
            
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 35, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 18, y: 13, width: 13, height: 8))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.rightView = vwForImg
            self.rightViewMode = UITextField.ViewMode.always
            
        }  else if rightPadding {
            let vwLeft : UIView = UIView(frame: CGRect(x: 0, y:0, width: 10, height: 35) )
            self.rightView = vwLeft
            self.rightViewMode = UITextField.ViewMode.always
        }
            
        if isBottomBorder {
            let bottomLayerEmail = CALayer()
            bottomLayerEmail.frame = CGRect(x: 0, y: self.frame.height-1, width: 1000, height: 0.6)
            bottomLayerEmail.backgroundColor = UIColor.darkGray.cgColor
            self.layer.addSublayer(bottomLayerEmail)
            self.clipsToBounds = true
        }
    }
    
    
    func setImagebothSide(imgs : [UIImage]) {
        
        let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 50, height: 35) )
        let imgVw : UIImageView = UIImageView(frame: CGRect(x: 18, y: 10, width: 19, height: 24))
        imgVw.image = imgs[0]
        vwForImg.addSubview(imgVw)
        self.leftView = vwForImg
        self.leftViewMode = UITextField.ViewMode.always
        
        
        let vwForImgR : UIView = UIView(frame: CGRect(x: 0, y:0, width: 35, height: 35) )
        let imgVwR : UIImageView = UIImageView(frame: CGRect(x: 0, y: 13, width: 13, height: 8))
        imgVwR.image = imgs[1]
        vwForImgR.addSubview(imgVwR)
        self.rightView = vwForImgR
        self.rightViewMode = UITextField.ViewMode.always
    }
    
    func setImageOnleftWidhtHeight(img : String ,leftPading : Bool, isBottomBorder : Bool, width1: Int, height1: Int) {
        
        if !img.isEmpty && leftPading {
            
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 50, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 18, y: 10, width: width1, height: height1))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
            
        } else if !img.isEmpty {
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 30, height: 35) )
            let imgVw : UIImageView = UIImageView(frame: CGRect(x: 5, y: 10, width: width1, height: height1))
            imgVw.image = UIImage(named: img)
            vwForImg.addSubview(imgVw)
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
            
        }  else if leftPading {
            let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 10, height: 35) )
            self.leftView = vwForImg
            self.leftViewMode = UITextField.ViewMode.always
        }
        
        if isBottomBorder {
            
            let bottomLayerEmail = CALayer()
            bottomLayerEmail.frame = CGRect(x: 0, y: self.frame.height-1, width: 1000, height: 0.6)
            bottomLayerEmail.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.layer.addSublayer(bottomLayerEmail)
            self.clipsToBounds = true
        }
    }
    func setBottomBorder(color : CGColor) {
        let bottomLayerEmail = CALayer()
        bottomLayerEmail.frame = CGRect(x: 0, y: self.frame.height-1, width: 1000, height: 0.6)
        bottomLayerEmail.backgroundColor = color // #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 1)
        self.layer.addSublayer(bottomLayerEmail)
        self.clipsToBounds = true
    }
    func setImagebothSideWidthHeight(imgs : [UIImage], width1 : Int, Height1 : Int) {
        
        let vwForImg : UIView = UIView(frame: CGRect(x: 0, y:0, width: 50, height: 35) )
        let imgVw : UIImageView = UIImageView(frame: CGRect(x: 18, y: 10, width: width1, height: Height1))
        imgVw.image = imgs[0]
        vwForImg.addSubview(imgVw)
        self.leftView = vwForImg
        self.leftViewMode = UITextField.ViewMode.always
        
        
        let vwForImgR : UIView = UIView(frame: CGRect(x: 0, y:0, width: 35, height: 35) )
        let imgVwR : UIImageView = UIImageView(frame: CGRect(x: 0, y: 13, width: 13, height: 8))
        imgVwR.image = imgs[1]
        vwForImgR.addSubview(imgVwR)
        self.rightView = vwForImgR
        self.rightViewMode = UITextField.ViewMode.always
    }
}

