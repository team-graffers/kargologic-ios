//  Created by Mac on 12/19/16.
//  Copyright © 2016 Ebabu IT Services. All rights reserved.

import UIKit
import SystemConfiguration
import Contacts
import AVFoundation
import CoreLocation
import UserNotifications
//import MBProgressHUD
//import Kingfisher

let ProgressMaxOffset : CGFloat = 1000000.0;

let ApplozicAppKey : String = "ad4f5b35888036f2bb391b9ecebace97"
//--- Google Map Kit API Key ---
let GoogleMapAPIKey : String = "AIzaSyADNAlEClkO38mY3L968R-NVnt3MsZed6U"

let DEVICE_ID = UIDevice.current.identifierForVendor?.uuidString

/*Colors Used In App*/
let HEADER_COLOR : UIColor = UIColor.init(hex: 0xed7787 , alpha: 1.0)

let SSPrefrenceIsLogin = "isUserLogin"
let SSPrefrenceLoginDetail = "userLoginDetails"
let SSPrefrenceSecretKey = "secretKey"
let SSPrefrenceDeviceToken = "deviceToken"
let SSPrefrenceUserLocation = "userLocation"
let SSPrefrenceChatNotification = "chatNotification"
let SSPrefrenceOtherNotification = "otherNotification"

@available(iOS 9.0, *)
var contactStore = CNContactStore()

struct AlertControllerFactory {
    
    static func alterControllerWithActions(_ title: String = "TextileTrade", message: String, style: UIAlertController.Style = .actionSheet, actions: UIAlertAction...) -> UIAlertController{
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: style)
        actions.forEach {
            controller.addAction($0)
        }
        
        return controller
    }
    static func action(_ title: String, style: UIAlertAction.Style, handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        
        return UIAlertAction(title: title, style: style, handler: handler)
    }
}
class AppTheme: NSObject {
    
    
    class func drawBorder(_ view: UIView, color: UIColor, borderWidth:CGFloat, cornerRadius:CGFloat) {
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
        view.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        view.clipsToBounds = true
    }
    
    /**** Requests For Access ****/
    
    class func requestForAccessCamera(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized:
            completionHandler(true)
            
        case .denied, .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (access) in
                completionHandler(access)
            })
            
        default:
            completionHandler(false)
        }
    }
    
    func requestForAccess(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        if #available(iOS 9.0, *) {
            let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
            switch authorizationStatus {
            case .authorized:
                completionHandler(true)
                
            case .denied, .notDetermined:
                contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
                    if access {
                        completionHandler(access)
                        
                    } else {
                        completionHandler(access)
                        //                        if authorizationStatus == CNAuthorizationStatus.denied {
                        //                            DispatchQueue.main.async(execute: { () -> Void in
                        //                                let message = "\(accessError!.localizedDescription)\n\nPlease allow the app to access your contacts through the Settings."
                        ////                                self.showMessage(message)
                        //                                print(message)
                        //                            })
                        //                        }
                    }
                })
                
            default:
                completionHandler(false)
            }
            
        } else {
            // Fallback on earlier versions
        }
    }
    
    /**** Alert For Access ****/
    
    class func alertToEncourageCameraAccessInitially()-> UIAlertController {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .default, handler: { (alert) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }))
        return alert
    }
    
    /*****************************************/
    
    func showMessage(_ message: String) {
        let alertController = UIAlertController(title: "MTJF", message: message, preferredStyle: UIAlertController.Style.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) -> Void in
            //            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }
        
        alertController.addAction(dismissAction)
        
        let pushedViewControllers = (AppDelegate().window?.rootViewController as! UINavigationController).viewControllers
        let presentedViewController = pushedViewControllers[pushedViewControllers.count - 1]
        
        presentedViewController.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: convert jsonString to Dictionary 
   class func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
    //Check Type of object
    class func navigationBackButton() -> UIBarButtonItem
    {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        return backItem
    }
    
    class func checkDictionary(_ anyDict : AnyObject) -> NSMutableDictionary
    {
        var returnDict : NSMutableDictionary = NSMutableDictionary()
        if let dict : NSMutableDictionary = anyDict as? NSMutableDictionary
        {
            if(dict.count > 0)
            {
                returnDict = dict.mutableCopy() as! NSMutableDictionary
            }
        }
        return returnDict
    }
    
    class func checkArray(_ anyArray : AnyObject) -> NSMutableArray
    {
        var returnArray : NSMutableArray = NSMutableArray()
        if let ary : NSMutableArray = anyArray as? NSMutableArray
        {
            if(ary.count > 0)
            {
                returnArray = ary.mutableCopy() as! NSMutableArray
            }
        }
        return returnArray
    }
    
    class func checkString(_ anyString : AnyObject) -> String
    {
        var returnString : String = ""
        if let str : String = anyString as? String
        {
            returnString = str
        }
        return returnString
    }
    
    class func checkInteger(_ anyString : AnyObject) -> NSInteger
    {
        var returnInteger : NSInteger = 0
        if let intt : NSInteger = anyString as? NSInteger
        {
            returnInteger = intt
        }
        else if let intt : String = anyString as? String
        {
            if(intt == "")
            {
                returnInteger = NSInteger(intt)!
            }
        }
        return returnInteger
    }
    
    class func checkBool(_ anyString : AnyObject) -> Bool
    {
        var returnBool : Bool = false
        if let booll : Bool = anyString as? Bool
        {
            returnBool = booll
        }
        else if let booll : String = anyString as? String
        {
            if(booll == "1")
            {
                returnBool = true
            }
            else if(booll == "0")
            {
                returnBool = false
            }
        }
        else if let booll : NSInteger = anyString as? NSInteger
        {
            if(booll == 1)
            {
                returnBool = true
            }
            else if(booll == 0)
            {
                returnBool = false
            }
        }
        return returnBool
    }
    
    class func setAppTitleImage() -> UIImageView
    {
        let logo = UIImage(named: "navigation-logo")
        let imageView = UIImageView(image:logo)
        
        return imageView
    }
    
    class  func isValidEmail(_ testStr:String) -> Bool {
        
        let emailRegEx = "^[_A-Za-z0-9]+(\\.[_A-Za-z0-9]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr) && !testStr.contains("__") && !testStr.contains("..")
    }
    class  func isValidGST(_ testStr:String) -> Bool {
        let gstRegEx = "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}Z[0-9A-Z]{1}?$"
        let gstTest = NSPredicate(format:"SELF MATCHES %@", gstRegEx)
        return gstTest.evaluate(with: testStr)
    }
    class func dateFromString(_ strDate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
        return dateFormatter.date(from: strDate)!
    }
    class func stdDateFromString(_ strDate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.date(from: strDate)!
    }
    class func timeFromString(_ strDate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss a"
        return dateFormatter.date(from: strDate)!
    }
    //MARK: —- ProgressHUD Global —-
//    class func showGlobalProgressHUDWithTitle(_ title: String) -> MBProgressHUD {
//        let window: UIWindow? = UIApplication.shared.windows.last!
//        MBProgressHUD.hide(for: window!, animated: true)
//        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: window!, animated: true)
//        hud.label.text = title
//        return hud
//    }
//    class func showGlobalProgressHUD() {
//        let window: UIWindow? = UIApplication.shared.windows.last!
//        MBProgressHUD.hide(for: window!, animated: true)
//        MBProgressHUD.showAdded(to: window!, animated: true)
//    }
//    class func showTostWithTitle(_ title: String) {
//        let window: UIWindow? = UIApplication.shared.windows.last!
//        MBProgressHUD.hide(for: window!, animated: true)
//        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: window!, animated: true)
//        hud.mode = .text
//        hud.label.text = title
//        hud.label.font = UIFont(name: "Roboto-Regular", size: 15)
//        hud.label.numberOfLines = 2
//
//        hud.label.textColor = UIColor.white
//        hud.bezelView.color = UIColor.init(hex: 000000, alpha: 1)
//        // hud.bezelView.frame = CGRect(x: 0.0, y: (window?.frame.height)!/3, width: 120, height: 7)
//        hud.bezelView.masksToBounds = true
//        hud.bezelView.cornerRadius = 5
//        hud.offset = CGPoint.init(x: 0.0, y: (window?.frame.height)!/2)
//        hud.hide(animated: true, afterDelay: 1.2)
//    }
    
//    class func showErrorTostWith(_ title: String) {
//        let window: UIWindow? = UIApplication.shared.windows.last!
//        MBProgressHUD.hide(for: window!, animated: true)
//        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: window!, animated: true)
//        hud.mode = .text
//        hud.label.text = title
//        hud.label.numberOfLines = 4
//        hud.label.textColor = UIColor.black
//        hud.hide(animated: true, afterDelay: 1.5)
//    }
    
//    class func dismissGlobalHUD() {
//        let window: UIWindow? = UIApplication.shared.windows.last!
//        MBProgressHUD.hide(for: window!, animated: true)
//    }
    
    // MARK: - Validation Methods
    class func isStringWithoutSpecialChar(_ strTerm : String)-> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
        if strTerm.rangeOfCharacter(from: characterset.inverted) != nil {
            //            print("string contains special characters")
            return false
        } else {
            return true
        }
    }
    
    //MARK:  AlertView
    class func showAlert(_ title:String , message : String , cancelButtonTitle:String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: cancelButtonTitle, style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(action)
        
        return alert
    }
  class func getMixedImg(image1: UIImage, image2: UIImage) -> UIImage {
        
    //let size = CGSize(image1.size.width, image1.size.height + image2.size.height)
    let size = CGSize(width: image1.size.width + image2.size.width , height: image1.size.height)
        
        UIGraphicsBeginImageContext(size)
        
//        image1.drawInRect(CGRectMake(0,0,size.width, image1.size.height))
        image1.draw(in: CGRect(x: 0, y: 0, width: size.width/2, height: size.height))
//        image2.drawInRect(CGRectMake(0,image1.size.height,size.width, image2.size.height))
        image2.draw(in: CGRect(x: size.width/2, y: 0, width: size.width/2, height: size.height))
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage!
    }
    // MARK :- Token Expire
    class func tokenExpired (msg : String) {
        
        let alertController = UIAlertController(title: "Alert", message: "Your session has expired. Please sign-in again.", preferredStyle: UIAlertController.Style.alert)
        let dismissAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) -> Void in
          
            SharedPreference.clearUserData()
//            SharedPreference.storeAuthToken("")
            let targetVC =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let navVC = UINavigationController(rootViewController: targetVC)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navVC
        }
        
        alertController.addAction(dismissAction)
        
        //        let pushedViewControllers = (AppDelegate().window?.rootViewController as! UINavigationController).viewControllers
        //        let presentedViewController = pushedViewControllers[pushedViewControllers.count - 1]
        //
        //        presentedViewController.present(alertController, animated: true, completion: nil)
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            // topController should now be your topmost view controller
            topController.present(alertController, animated: true, completion: nil)
        }
    }
    
   class func isConnectedToNetwork() -> Bool {
//
//        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
//        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
//            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
//                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
//            }
//        }
//
//        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
//        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
//            return false
//        }
//
//        let isReachable = flags == .reachable
//        let needsConnection = flags == .connectionRequired
//
//    print(isReachable)
//    print(needsConnection)
//
//        return isReachable && !needsConnection
    
    
    var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
    if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
        return false
    }
    
    /* Only Working for WIFI
     let isReachable = flags == .reachable
     let needsConnection = flags == .connectionRequired
     
     return isReachable && !needsConnection
     */
    
    // Working for Cellular and WIFI
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    let ret = (isReachable && !needsConnection)
    
    return ret
    
    }
    
    // MARK: - Rate & Version update Method
    class func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }

    //convert string to image
    class func base64ToImage(toImage strEncodeData: String) -> UIImage {
        let dataDecoded  = NSData(base64Encoded: strEncodeData, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
        let image = UIImage(data: dataDecoded as Data)
        return image!
    }
    
}

extension NSAttributedString {
    public func attributedStringByTrimmingCharacterSet(charSet: NSCharacterSet) -> NSAttributedString {
        let modifiedString = NSMutableAttributedString(attributedString: self)
        modifiedString.trimCharactersInSet(charSet: charSet)
        return NSAttributedString(attributedString: modifiedString)
    }
}

extension NSMutableAttributedString {
    public func trimCharactersInSet(charSet: NSCharacterSet) {
        var range = (string as NSString).rangeOfCharacter(from: charSet as CharacterSet)
        
        // Trim leading characters from character set.
        while range.length != 0 && range.location == 0 {
            replaceCharacters(in: range, with: "")
            range = (string as NSString).rangeOfCharacter(from: charSet as CharacterSet)
        }
        
        // Trim trailing characters from character set.
        range = (string as NSString).rangeOfCharacter(from: charSet as CharacterSet, options: .backwards)
        while range.length != 0 && NSMaxRange(range) == length {
            replaceCharacters(in: range, with: "")
            range = (string as NSString).rangeOfCharacter(from: charSet as CharacterSet, options: .backwards)
        }
    }
}
public enum ImageFormat {
    case PNG
    case JPEG(CGFloat)
}
extension UIImage {
    //convert image to base64 string
    func toBase64(format : ImageFormat ) -> String {
        var imageData: NSData
        switch format {
        case .PNG: imageData = self.pngData()! as NSData
        case .JPEG(let compression): imageData = self.jpegData(compressionQuality: 0.75)! as NSData
        }
        return imageData.base64EncodedString(options: .lineLength64Characters)
    }
}
// MARK: - Network Reachability Method
protocol Utilities {
}

extension NSObject : Utilities{
    
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
}


extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        //Return Result
        return isLess
    }
}

typealias UnixTime = Int64
extension UnixTime {
    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    var dateFull: NSDate {
        return NSDate(timeIntervalSince1970: Double(self)/1000.0)
    }
    var toHour: String {
        return formatType(form: "HH:mm").string(from: dateFull as Date)
    }
    var toDay: String {
        return formatType(form: "dd MMM yyyy").string(from: dateFull as Date)
    }
    var fullTime : String {
        
        return formatType(form: "dd-MMM-yyyy H:mm a").string(from: dateFull as Date)
    }
}


// MARK: - extension - UIColor
extension UIColor {
    
    convenience init(hex:Int, alpha:CGFloat = 1.0) {
        self.init(
            red:   CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((hex & 0x0000FF) >> 0)  / 255.0,
            alpha: alpha
        )
    }
    
}

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    func getShorNameFromString() -> String {
        let stringArray = self.components(separatedBy: " ")
        
        if stringArray.count == 1 {
            
            print(stringArray[0].prefix(2).uppercased())
            
            return stringArray[0].prefix(2).uppercased()
        }else if stringArray.count > 1 {
           
            var strArr = [String]()
            
            for item in stringArray{
                if item != ""{
                   strArr.append(item)
                }
            }
            return (strArr[0].first?.uppercased())! + (strArr[1].first?.uppercased())!
        }else{
            return "NA"
        }
    }
}

extension UIViewController {
    
    //Previous code
    
    var activityIndicatorTag: Int { return 999999 }
    
    
    func startActivityIndicator(
        style: UIActivityIndicatorView.Style = .whiteLarge,
        location: CGPoint? = nil) {
        
        //Set the position - defaults to `center` if no`location`
        
        //argument is provided
        
        let loc = location ?? self.view.center
        
        //Ensure the UI is updated from the main thread
        
        //in case this method is called from a closure
        
        DispatchQueue.main.async(execute: {
            
            
            //Create the activity indicator
            
            let activityIndicator = UIActivityIndicatorView(style: style)
            //Add the tag so we can find the view in order to remove it later
            
            activityIndicator.tag = self.activityIndicatorTag
            activityIndicator.color = #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1)
            //Set the location
            
            activityIndicator.center = loc
            activityIndicator.hidesWhenStopped = true
            //Start animating and add the view
            
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
            self.view.isUserInteractionEnabled = false
        })
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async(execute: {
            
            if let activityIndicator = self.view.subviews.filter(
                { $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
                self.view.isUserInteractionEnabled = true
                
            }
        })
    }
    
    func animateIn(popUp : UIView) {
        self.navigationController?.view.addSubview(popUp)
        popUp.center = self.view.center
        let newFrame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: (self.navigationController?.view.frame.size.height)!)
        popUp.frame = newFrame
        popUp.transform =  CGAffineTransform.init().scaledBy(x: 1.3, y: 1.3)
        popUp.alpha = 0
        UIView.animate(withDuration: 0, animations: {
            popUp.alpha = 1
            popUp.transform = CGAffineTransform.identity
        })
    }
    func animateOut(popUp : UIView) {
        popUp.removeFromSuperview()
    }
     func openURL(with urlString: String){
        guard let url = URL(string: urlString) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
//    func addFloatyButton(isDashboard: Bool) {
//         let floaty = Floaty()
//        floaty.buttonColor = #colorLiteral(red: 0.01176470588, green: 0.3058823529, blue: 0.9058823529, alpha: 1)
//        floaty.buttonImage = UIImage(named: "custom-add")
//        if SharedPreference.getUserData().UserType == Traveller {
//            floaty.addItem("My Buddy", icon: UIImage(named: "myBuddy"), titlePosition: .left) { (item) in
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyBuddyVC") as! MyBuddyVC
//                vc.isFromWhere = "myBuddy"
//                self.navigationController?.pushViewController(vc, animated: true)
//                floaty.close()
//            }
//        }else{
//            floaty.addItem("My Bookings", icon: UIImage(named: "calenderWhite"), titlePosition: .left) { (item) in
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyBuddyVC") as! MyBuddyVC
//                vc.isFromWhere = "myBookings"
//                self.navigationController?.pushViewController(vc, animated: true)
//                floaty.close()
//            }
//        }
//        floaty.addItem("Recent Chat", icon: UIImage(named: "chat"), titlePosition: .left) { (item) in
//            let userservice = ALUserService()
//            userservice.getUserDetail(SharedPreference.getUserData().UniqueId, withCompletion: { (contact) in
//                let chatManager : ALChatManager =  ALChatManager(applicationKey: ALChatManager.applicationId as NSString)
//                chatManager.launchChat(self)
//            })
//            floaty.close()
//        }
//        floaty.sticky = true
//        //        floaty.paddingX = self.view.frame.width/8 - floaty.frame.width/2
//        //        fab.openAnimationType = .s
//        if isDashboard {
//           floaty.paddingY = 50
//        }
//
////        floaty.fabDelegate = (self as! FloatyDelegate)
//
//        //        print(tableView!.frame)
//        //        lblUnreadCount = UILabel(frame: CGRect(x: self.view.frame.width - 25, y: self.view.frame.height - 80, width: 24, height: 24))
//        //        lblUnreadCount.backgroundColor = #colorLiteral(red: 1, green: 0.1058823529, blue: 0.368627451, alpha: 1)
//        //        lblUnreadCount.cornerRadius = lblUnreadCount.frame.width/2
//        //        lblUnreadCount.textAlignment = .center
//        //        lblUnreadCount.textColor = UIColor.white
//        //
//        //        lblUnreadCount.masksToBounds = true
//        self.view.addSubview(floaty)
//        //        self.view.addSubview( lblUnreadCount)
//    }
    // MARK: - Floaty Delegate Methods
    // extension HomeVC : FloatyDelegate {
    //
    //    func floatyWillOpen(_ floaty: Floaty) {
    //        print("Floaty Will Open")
    //    }
    //
    //    func floatyDidOpen(_ floaty: Floaty) {
    //        print("Floaty Did Open")
    //    }
    //
    //    func floatyWillClose(_ floaty: Floaty) {
    //        print("Floaty Will Close")
    //    }
    //
    //    func floatyDidClose(_ floaty: Floaty) {
    //        print("Floaty Did Close")
    //    }
    //
    // }
}
extension String
{
    func isStringAnInt() -> Bool {
        
        if let _ = Int(self) {
            return true
        }
        return false
    }
}
extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
extension UIImage {
    class func imageWithLabel(label: UILabel) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0.0)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    class func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 0.5)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

   func cropedToRatio(ratio: CGFloat) -> UIImage? {
            let newImageWidth = size.height * ratio
            
            let cropRect = CGRect(x: ((size.width - newImageWidth) / 2.0) * scale,
                                  y: 0.0,
                                  width: newImageWidth * scale,
                                  height: size.height * scale)
            
            guard let cgImage = cgImage else {
                return nil
            }
            guard let newCgImage = cgImage.cropping(to: cropRect) else {
                return nil
            }
            
            return UIImage(cgImage: newCgImage, scale: scale, orientation: imageOrientation)
        }
    
}
extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
}
class ScaledHeightImageView: UIImageView {
    
    override var intrinsicContentSize: CGSize {
        
        if let myImage = self.image {
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width
            
            let ratio = myViewWidth/myImageWidth
            let scaledHeight = myImageHeight * ratio
            
            return CGSize(width: myViewWidth, height: scaledHeight)
        }
        
        return CGSize(width: -1.0, height: -1.0)
    }
    
}
