
import UIKit
//import KYDrawerController

protocol ViewControllerIdentifier{
    static var identifier:String {get}
}

extension UIViewController{
    var titleString:String {
        switch  self {
            
        default:
            return ""
        }
    }
}
extension UIViewController {
    func popViewController() -> UIViewController?{
        return self.navigationController?.popViewController(animated:true)
    }
    
    func showNavigation() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func hideNavigation() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    func hideBackButton(){
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
    func showBackButton()   {
        self.navigationController?.navigationItem.hidesBackButton = false
    }
    
    
    func hideTabBar(){
        self.tabBarController?.tabBar.isHidden = true
    }
    func showTabBar(){
        self.tabBarController?.tabBar.isHidden = false
    }
}
extension BaseViewController: ViewControllerIdentifier {
    static var identifier: String {
        return String(describing: self)
    }
}

class BaseViewController: UIViewController {
    
    
    func updateTitle() {
        DispatchQueue.main.async {
            self.navigationItem.title = self.titleString
        }
    }
    
    @IBAction func backSegueActions(sender: UIStoryboardSegue){
        self.navigationController?.popViewController(animated: true)
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  self.view.layer.contents = UIImage(named: "bg")?.cgImage
        //  self.view.layer.contentsGravity = kCAGravityResize
        
        self.navigationItem.title = self.titleString
        self.edgesForExtendedLayout = []
        
        updateTitle()
    }
}


extension UIViewController {
    
    func goToHomePage() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeView = storyboard.instantiateViewController(withIdentifier: "MainTabVC") as! UITabBarController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        // let tabBarController = appDelegate.window?.rootViewController as? UITabBarController
        appDelegate.window?.rootViewController = homeView;
    }


    func displayContentController(content: UIViewController, animation: Bool = true) {
        
        addChild(content)
        content.view.frame = self.view.bounds
        self.view.addSubview(content.view)
        content.view.isHidden = true
        if animation {
            UIView.animate(withDuration: 0.5) {
                content.view.isHidden = false
            }
        }else {
            content.view.isHidden = false
        }
        content.didMove(toParent: self)
    }
    
    func displayContentControllerToMainWindow(content: UIViewController, animation: Bool = true) {
        
        
        addChild(content)
        content.view.frame = self.view.bounds
        
        
        self.view.addSubview(content.view)
        content.view.isHidden = true
        if animation {
            UIView.animate(withDuration: 0.5) {
                content.view.isHidden = false
            }
        }else {
            content.view.isHidden = false
        }
        content.didMove(toParent: self)
    }
    
    func hideContentController(content: UIViewController) {
        content.willMove(toParent: nil)
        //        var rect = content.view.frame
        //        rect.origin.x = -rect.size.width
        UIView.animate(withDuration: 0.5, animations: {
            //            content.view.frame = rect
            content.view.isHidden = true
        }) { (status) in
            content.view.removeFromSuperview()
            content.removeFromParent()
        }
    }
    func logout() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeView = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        // let tabBarController = appDelegate.window?.rootViewController as? UITabBarController
        appDelegate.window?.rootViewController = homeView;
        SharedPreference.clearUserData()
//        SharedPreference.storeAuthToken("")
        
    }
}

extension CAGradientLayer {
    
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0, y: 1)
        endPoint = CGPoint(x: 1, y: 0)
    }
    
    func createGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
}

extension UINavigationBar {
    
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.createGradientImage(), for: UIBarMetrics.default)
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}
