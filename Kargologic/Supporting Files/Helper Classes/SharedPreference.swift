

import UIKit
//import Applozic

class SharedPreference: NSObject {
    
   
    
    fileprivate let kCarrID = "__u_carr_id"
    fileprivate let kCarrComId = "__u_carr_company_id"
    fileprivate let kUserEmail = "__u_email"
    fileprivate let kUserName = "__u_nm"
    fileprivate let kProfileImage = "__u_profile_img"
    fileprivate let kToken = "__u_token"
    fileprivate let kMessage = "__k_message";
    fileprivate let kPhoneNumber = "__u_mobile_number"
    
    fileprivate let kTrackingId = "__u_tracking_id"
    fileprivate let kDeviceId = "__u_device_id"
    fileprivate let kDeviceSecret = "__u_device_secret"
    fileprivate let kLicenseNum = "__u_license_number"
    fileprivate let kCategName = "__u_cat_name"
    fileprivate let kCateId = "__u_cate_id"
    fileprivate let kTruckRegNum = "__u_truck_reg_number"
    
 
    fileprivate let defaults = UserDefaults.standard
    static let sharedInstance = SharedPreference()
    

    class func saveUserData(_ user: UserEntity) {
        sharedInstance.saveUserData(user)
    }
    
    fileprivate func saveUserData(_ user: UserEntity) {
        
        defaults.setValue(user.carr_id  , forKey: kCarrID)
        defaults.setValue(user.name, forKey: kUserName)
        defaults.setValue(user.email, forKey: kUserEmail)
        defaults.setValue(user.carr_com_id, forKey: kCarrComId)
        defaults.setValue(user.phone, forKey: kPhoneNumber)
        defaults.setValue(user.token, forKey: kToken)
        defaults.setValue(user.message, forKey: kMessage)
        defaults.setValue(user.image, forKey: kProfileImage)
        
        defaults.setValue(user.trackingId, forKey: kTrackingId)
        defaults.setValue(user.deviceId, forKey: kDeviceId)
        defaults.setValue(user.deviceSecret, forKey: kDeviceSecret)
        defaults.setValue(user.license_num, forKey: kLicenseNum)
        defaults.setValue(user.categ_name, forKey: kCategName)
        defaults.setValue(user.cate_id, forKey: kCateId)
        defaults.setValue(user.truck_reg_num, forKey: kTruckRegNum)
        

        defaults.synchronize()
    }
  
    
    class func clearUserData(){
        sharedInstance.clearUserData()
    }
    
    fileprivate func clearUserData(){
        self.saveUserData(UserEntity())
    }
    
    class func getUserData() -> UserEntity {
        return sharedInstance.getUserData()
    }
    
    fileprivate  func getUserData() -> UserEntity {
        
        let user:UserEntity      =  UserEntity()
        user.token             =  defaults.value(forKey: kToken)  as? String ?? ""
        user.carr_id    =  defaults.value(forKey: kCarrID)  as? Int ?? 0
        user.carr_com_id                = defaults.value(forKey: kCarrComId) as? Int ?? 0
        user.name                =  defaults.value(forKey: kUserName) as? String  ?? ""
        user.image           =  defaults.value(forKey: kProfileImage) as? String ?? ""
        user.phone            =  defaults.value(forKey: kPhoneNumber) as? String ?? ""
        user.email                  = defaults.value(forKey: kUserEmail) as? String ?? ""
        user.message               =  defaults.value(forKey: kMessage) as? String ?? ""
        
        user.trackingId               =  defaults.value(forKey: kTrackingId) as? String ?? ""
        user.deviceId               =  defaults.value(forKey: kDeviceId) as? String ?? ""
        user.deviceSecret               =  defaults.value(forKey: kDeviceSecret) as? String ?? ""
        user.license_num               =  defaults.value(forKey: kLicenseNum) as? String ?? ""
        user.categ_name               =  defaults.value(forKey: kCategName) as? String ?? ""
        user.cate_id               =  defaults.value(forKey: kCateId) as? Int ?? 0
        user.truck_reg_num               =  defaults.value(forKey: kTruckRegNum) as? String ?? ""
       
        return user
    }
    
    // device token
    class func storeDeviceToken(_ token: String) {
        sharedInstance.setDeviceToken(token)
    }
    class func deviceToken() -> String {
        return sharedInstance.getDeviceToken() ?? ""
    }
    fileprivate func setDeviceToken(_ token: String){
        defaults.set(token, forKey: kDeviceToken);
    }
    fileprivate func getDeviceToken() -> String?{
        return defaults.value(forKey: kDeviceToken) as? String;
    }
    
    // AuthToken
    class func storeAuthToken(_ token: String) {
        sharedInstance.setAuthToken(token)
    }
    class func authToken() -> String {
        return sharedInstance.getAuthToken() ?? ""
    }
    fileprivate func setAuthToken(_ token: String){
        defaults.set(token, forKey: kToken);
    }
    fileprivate func getAuthToken() -> String?{
        return defaults.value(forKey: kToken) as? String;
    }

}
