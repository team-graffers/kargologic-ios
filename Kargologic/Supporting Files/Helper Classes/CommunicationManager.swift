
import UIKit
import MobileCoreServices

class CommunicationManager: NSObject {
    
    let notificationName = Notification.Name("login")
    let defaults = UserDefaults.standard
    
    //TODO: App Transport Layer Security
    //TODO: Check all the alerts
    
    public enum WebServiceType :Int {
        
        // Kargologic Driver APIs
        case Login
        case Dashboard
        case ViewMore
        case ChangeOrderStatus
        case UpdateDropActualTime
        case UpdatePickupActualTime
        case GetNotification
        case CommentSend
        case UpdateProfile
        case GetTruckCatList
        case UpdatePassword
        case ForgotPass
        case UpdateUserLocation
        case UpdateEmail
        case UniqueNumber
        
        
        
        func getRequestURLByType() -> String {
            
            switch self {
                
            case .Login:                        return "\(APPURL.dev)driv/driv_login/"
            case .Dashboard:                   return "\(APPURL.dev)driv/driver_dashboard/?status="
            case .ViewMore:                   return "\(APPURL.dev)driv/driver_job_detail/?order_id="
            case .ChangeOrderStatus:     return "\(APPURL.dev)orders/driver_chng_ord_stat_/?en_route_or_pod="
            case .UpdateDropActualTime:                   return "\(APPURL.dev)delivery/driver_update_drp_actual_time/"
            case .UpdatePickupActualTime: return "\(APPURL.dev)pickup/driver_update_pck_actual_time/"
            case .GetNotification:       return "\(APPURL.dev)driv/notify-driv/"
            case .CommentSend:        return "\(APPURL.dev)driv/driver_order_comment/"
            case .UpdateProfile:        return "\(APPURL.dev)driv/driver_update_profile/"
            case .GetTruckCatList:        return "\(APPURL.dev)truck_categ/truck_categ_list/"
            case .UpdatePassword:        return "\(APPURL.dev)driver_reset_pass/"
            case .ForgotPass:           return "\(APPURL.dev)driv_forgot_pass/"
            case .UpdateUserLocation:   return "\(APPURL.dev)cor_driver_update/"
            case .UpdateEmail:           return "\(APPURL.dev)driver_pr_upd_email_chk/"
            case .UniqueNumber:         return "\(APPURL.dev)unique_user_num/"

                
            }
        }
    }
    
    // MARK: - Put/Json
    func getResponseForPut(service : WebServiceType, parameters: NSDictionary? , completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        if AppTheme.isConnectedToNetwork() {
            
            let request = NSMutableURLRequest(url: URL(string: service.getRequestURLByType())!)
            print("URL----->>  ", request.url!)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let secretKey = "Token " + SharedPreference.authToken()
            request.setValue(secretKey, forHTTPHeaderField: "Authorization")
            
            if parameters != nil {
                print("Param--------->>>  ",parameters)
                var data : Data?
                do {
                    data = try JSONSerialization.data(withJSONObject: parameters!, options: JSONSerialization.WritingOptions(rawValue: 0))
                    request.httpBody = data
                } catch {
                    data = Data()
                }
            }
            self.callWebservice(request as URLRequest?) { (responseData, responseCode) in
                
                DispatchQueue.main.async(execute: {
                    if responseData is NSDictionary {
                        
                        if responseCode == "200" {
                            completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            completion("400", responseData as AnyObject)
                        }
                        
                    }else {
                        completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
    // MARK: - GET/Json
    func getResponseFor(service : WebServiceType, parameters: String , completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        if AppTheme.isConnectedToNetwork() {
//            let escapedAddress = parameters.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            var serviceStr = "\(service.getRequestURLByType())?\(parameters)"
            
            var serviceStr = service.getRequestURLByType()+parameters
            serviceStr = serviceStr.replacingOccurrences(of: " ", with: "%20")
            
            let request = NSMutableURLRequest(url: URL(string: serviceStr)!)
            print("URL----->>  ", request.url!)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            let secretKey = "Token " + SharedPreference.authToken()
            print("Authorization ------>", secretKey)
            //        if AppTheme.getIsUserLogin() {
            //            let secretKey = AppTheme.getSecretKey()
            request.setValue(secretKey, forHTTPHeaderField: "Authorization")
            //        }
            
            self.callWebservice(request as URLRequest?) { (responseData, responseCode) in
                
                DispatchQueue.main.async(execute: {
                    if responseData is NSDictionary {
                        
                        if responseCode == "200" {
                            completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            completion("400", responseData as AnyObject)
                        }
                        
                    }else {
                        completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
    
    // MARK: - Get/ Query param
    func getResponseForQueryParam(service : WebServiceType, urlParameters: String,reqParameters: NSDictionary?, completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        if AppTheme.isConnectedToNetwork() {
            
            var serviceStr = "\(service.getRequestURLByType())\(urlParameters)"
            serviceStr = serviceStr.replacingOccurrences(of: " ", with: "%20")
            
            
            let request = NSMutableURLRequest(url: URL(string: serviceStr)!)
            print("URL----->>  ", request.url!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            if !SharedPreference.authToken().isEmpty {
                let secretKey = "Token " + SharedPreference.authToken()
                print("Authorization ------>", secretKey)
                request.setValue(secretKey, forHTTPHeaderField: "Authorization")
            }
            if reqParameters != nil {
                print("Param--------->>>  ",reqParameters)
                var data : Data?
                do {
                    data = try JSONSerialization.data(withJSONObject: reqParameters!, options: JSONSerialization.WritingOptions(rawValue: 0))
                    request.httpBody = data
                } catch {
                    data = Data()
                }
            }
            self.callWebservice(request as URLRequest?) { (responseData, responseCode) in
                
                DispatchQueue.main.async(execute: {
                    if responseData is NSDictionary {
                        
                        if responseCode == "200" {
                            completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            completion("400", responseData as AnyObject)
                        }
                        
                    }else {
                        completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
    
    
    // MARK: - Post/Json
    func getResponseForPost(service : WebServiceType, parameters: NSDictionary? , completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        
        if AppTheme.isConnectedToNetwork() {
            
            let request = NSMutableURLRequest(url: URL(string: service.getRequestURLByType())!)
            print("URL----->>  ", request.url!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            //        if AppTheme.getIsUserLogin() {
            //            let secretKey = AppTheme.getSecretKey()
            if !SharedPreference.authToken().isEmpty {
                let secretKey = "Token " + SharedPreference.authToken()
                print("Authorization ------>", secretKey)
                request.setValue(secretKey, forHTTPHeaderField: "Authorization")
            }
            
            //        }
            
            if parameters != nil {
                print("Param--------->>>  ",parameters)
                var data : Data?
                do {
                    data = try JSONSerialization.data(withJSONObject: parameters!, options: JSONSerialization.WritingOptions(rawValue: 0))
                    request.httpBody = data
                } catch {
                    data = Data()
                }
            }
            self.callWebservice(request as URLRequest?) { (responseData, responseCode) in
                
                DispatchQueue.main.async(execute: {
                    if responseData is NSDictionary {
                        
                        if responseCode == "200" {
                            completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            completion("400", responseData as AnyObject)
                        }
                        
                    }else {
                        completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
    // MARK: - Post/Param
    func getResponseForParamType(service : WebServiceType, parameters: NSDictionary?, completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        if AppTheme.isConnectedToNetwork() {
            
            let body = NSMutableData()
            let request = NSMutableURLRequest(url: NSURL(string: service.getRequestURLByType())! as URL)
            print("URL----->>  ", request.url!)
            request.httpMethod = "POST";
            if !SharedPreference.authToken().isEmpty {
                let secretKey = "Token " + SharedPreference.authToken()
                request.setValue(secretKey, forHTTPHeaderField: "Authorization")
            }
            let boundary = generateBoundaryString()
            //define the multipart request type
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            if parameters != nil {
                print("Param--------->>>  ",parameters)
                for (key, value) in parameters! {
                    body.append(Data( "--\(boundary)\r\n".utf8))
                    body.append(Data( "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                    body.append(Data( "\(value)\r\n".utf8))
                }
            }
            body.append(Data( "--\(boundary)--\r\n".utf8))
            request.httpBody = body as Data
            
            self.callWebservice(request as URLRequest?) { (responseData, responseCode) in
                DispatchQueue.main.async(execute: {
                    if responseData is NSDictionary {
                        
                        if responseCode == "200" {
                            completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            completion("400", responseData as AnyObject)
                        }
                        
                    }else {
                        completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
    // MARK: - Post/Param (Multipart- Image)
    //My function for sending data with image AAKASH VISHWAKARMA
    func getResponseForMultipartType(service : WebServiceType, parameters: NSDictionary?, fileData : [Data], fileURLs : [URL], fileKeys: [String] ,  completion:@escaping (_ result: String, _ data : AnyObject)->()) {
        if AppTheme.isConnectedToNetwork() {
            //filePath
            let request = NSMutableURLRequest(url: NSURL(string: service.getRequestURLByType())! as URL)
            print("URL----->>  ", request.url!)
            request.httpMethod = "POST";
            if !SharedPreference.authToken().isEmpty {
                let secretKey = "Token " + SharedPreference.authToken()
                request.setValue(secretKey, forHTTPHeaderField: "Authorization")
            }
            
            
            let boundary = generateBoundaryString()
            //define the multipart request type
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            //        let imageData = UIImageJPEGRepresentation(userimage, 1)
            //        if(imageData==nil)  { return }
            
            request.httpBody = createBodyWithParameters(parameters: parameters, fileKeys: fileKeys, fileURLs : fileURLs, fileData: fileData, boundary: boundary) as Data
            self.callWebservice(request as URLRequest?) { (responseData, responseCode) in
                
                DispatchQueue.main.async(execute: {
                    if responseData is NSDictionary {
                        
                        if responseCode == "200" {
                            completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            completion("400", responseData as AnyObject)
                        }
                        
                    }else {
                        completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
                
            }
        }else {
            completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
    //    func createBodyWithParameters(parameters: NSDictionary?, imageKey: [String], imageData: [Data], boundary: String) -> NSData {
    //
    //        let body = NSMutableData();
    //        if parameters != nil {
    //            for (key, value) in parameters! {
    //                body.append(Data("--\(boundary)\r\n".utf8))
    //                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
    //                body.append(Data("\(value)\r\n".utf8))
    //            }
    //        }
    //
    //        if !imageData.isEmpty {
    //            for i in 0...imageData.count-1 {
    //                if !imageData[i].isEmpty {
    //                    let filename = "user-profile\(i)"
    //                    let mimetype = "image/jpg"
    //
    //                    body.append(Data( "--\(boundary)\r\n".utf8))
    //                    body.append(Data( "Content-Disposition: form-data; name=\"\(imageKey[i])\"; filename=\"\(filename)\"\r\n".utf8))
    //                    body.append(Data( "Content-Type: \(mimetype)\r\n\r\n".utf8))
    //                    body.append(imageData[i] as Data)
    //                    body.append(Data( "\r\n".utf8))
    //                    body.append(Data( "--\(boundary)--\r\n".utf8))
    //                }
    //            }
    //        }
    //
    //        return body
    //    }
    
    /*New Method*/
    func createBodyWithParameters(parameters: NSDictionary?, fileKeys: [String], fileURLs : [URL], fileData: [Data], boundary: String) -> NSData {
        
        let body = NSMutableData();
        if parameters != nil {
            for (key, value) in parameters! {
                body.append(Data("--\(boundary)\r\n".utf8))
                body.append(Data("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".utf8))
                body.append(Data("\(value)\r\n".utf8))
            }
        }
        //        if fileData.isEmpty {
        //            imgURL = nil
        //        }
        
        if !fileURLs.isEmpty {
            for i in 0...fileURLs.count-1 {
                //                do {
                //                    let fileData = try NSData(contentsOf: fileURLs[i] , options: NSData.ReadingOptions()) as Data
                
                if !fileData[i].isEmpty {
                    let filename = fileURLs[i]
                    
                    let mimetype = fileURLs[i].mimeType() //"image/jpg"
                    
                    body.append(Data( "--\(boundary)\r\n".utf8))
                    body.append(Data( "Content-Disposition: form-data; name=\"\(fileKeys[i])\"; filename=\"\(filename)\"\r\n".utf8))
                    body.append(Data( "Content-Type: \(mimetype)\r\n\r\n".utf8))
                    body.append(fileData[i] as Data)
                    body.append(Data( "\r\n".utf8))
                    body.append(Data( "--\(boundary)--\r\n".utf8))
                }
                //                }   catch {
                //                    print(error)
                //                }
            }
        }
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func downloadImage(fromURL: URL,completion:@escaping (_ responseData : AnyObject) -> ()) {
        let task = URLSession.shared.dataTask(with: fromURL, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                let errorDictionary = NSMutableDictionary()
                errorDictionary.setValue("0", forKey: WebServiceKey.Status.rawValue)
                errorDictionary.setValue(error!.localizedDescription, forKey: WebServiceKey.Message.rawValue)
                completion(errorDictionary)
            } else {
                completion(data as AnyObject)
            }
        })
        task.resume()
    }
    
    
    func callWebservice(_ request: URLRequest!, completion:@escaping (_ responseData : NSDictionary, _ responseCode : String) -> ()) {
        
        let task = URLSession.shared
            .dataTask(with: request, completionHandler: {
                (data, response, error) -> Void in
                if let httpResponse = response as? HTTPURLResponse {
                    print("statusCode: \(httpResponse.statusCode)")
                    
                    if httpResponse.statusCode == 401 || httpResponse.statusCode == 403{
                        AppTheme.tokenExpired(msg: "") // when session expired or login from other device
                    }
                    
                    if (error != nil) {
                        print(error!.localizedDescription)
                        let errorDictionary = NSMutableDictionary()
                        //                    errorDictionary.setValue("400", forKey: WebServiceKey.Status.rawValue)
                        errorDictionary.setValue(error!.localizedDescription, forKey: WebServiceKey.Message.rawValue)
                        completion(errorDictionary, "400")
                    } else {
                        
                        do {
                            //print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                            let parsedJSON = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions(rawValue: 0))
                            completion((parsedJSON as AnyObject) as! NSDictionary, "\(httpResponse.statusCode)")
                        } catch let JSONError as NSError {
                            let errorDictionary = NSMutableDictionary()
                            //                        errorDictionary.setValue("400", forKey: WebServiceKey.Status.rawValue)
                            errorDictionary.setValue(JSONError, forKey: WebServiceKey.Message.rawValue)
                            completion(errorDictionary, "400")
                        }
                    }
                }
            })
        task.resume()
    }
    
    func showAlert(title:String , message : String , cancelButtonTitle:String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: cancelButtonTitle, style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(action)
        
        return alert
    }
    
    
    
}
