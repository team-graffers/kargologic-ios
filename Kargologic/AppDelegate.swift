//
//  AppDelegate.swift
//  Kargologic
//
//  Created by mac on 25/04/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit
import CoreLocation
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

      lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        manager.allowsBackgroundLocationUpdates = true
        return manager
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        UIApplication.shared.statusBarView?.backgroundColor = #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1)
        UIApplication.shared.statusBarStyle = .lightContent
        locationManager.startUpdatingLocation()

        IQKeyboardManager.sharedManager().enable = true
        
        if !SharedPreference.authToken().isEmpty {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeView = storyboard.instantiateViewController(withIdentifier: "MainTabVC") as! UITabBarController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            // let tabBarController = appDelegate.window?.rootViewController as? UITabBarController
            appDelegate.window?.rootViewController = homeView;
        }
        
        
//        var gameTimer: Timer!
//        gameTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        
        return true
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//                var gameTimer: Timer!
//                gameTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
        
               // gameTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

// MARK: - CLLocationManagerDelegate
extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let mostRecentLocation = locations.last else {
            return
        }
        
        
        print( SharedPreference.authToken())
        
        
        let param = ["lat": mostRecentLocation.coordinate.latitude,
                     "lng": mostRecentLocation.coordinate.longitude
        ]

        if SharedPreference.authToken() == ""{
            self.locationManager.stopUpdatingHeading()
        }else{
            
            CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.UpdateUserLocation, parameters: param as NSDictionary) { (result, data) in
                
                if (result == "200") {
                    print("Send to server \(param)")
                }else{
                    DispatchQueue.main.async { self.locationManager.stopUpdatingHeading()
                        self.locationManager.allowsBackgroundLocationUpdates  = false}
                }
                
            }
        }
       
    
    }
}

    

