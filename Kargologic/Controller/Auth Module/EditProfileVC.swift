//
//  EditProfileVC.swift
//  Kargologic
//
//  Created by sachin jain on 07/05/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit
import Photos

class EditProfileVC: UIViewController {

   // @IBOutlet weak var userImgVw: UIImageView!
    @IBOutlet weak var phoneNumTextField : UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var licenseTextField: UITextField!{
        didSet{
            self.licenseTextField.delegate = self
        }
    }
    @IBOutlet weak var vehicleTypeTextField: UITextField!
    @IBOutlet weak var vehicleRegTextField: UITextField!{
        didSet{
            self.vehicleRegTextField.delegate = self
        }
    }
    
    var isEmail = false
    
    @IBOutlet weak var confirmUpdatePassVw: UIView!
    @IBOutlet weak var updatePassImgVw: UIImageView!
    @IBOutlet weak var updatePassTxtFld: UITextField!
    
    
    @IBOutlet var updateView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var updateImgVw: UIImageView!
    @IBOutlet weak var upddateImgVw2: UIImageView!
    @IBOutlet weak var placeHolder1TxtFld: UITextField!
    @IBOutlet weak var placeHolder2TxtFld: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var updateErrorLbl: UILabel!
    
    
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var btnUpdate: GradientBtn!
    @IBOutlet weak var updateBtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var editImgVw: UIButton!
    @IBOutlet weak var viewTruckType: UIView!
    @IBOutlet weak var vehicleTypeTableView: UITableView!
    
    var isEmailUpdate = false
    var isphonUpdate = false
    var isLicenUpdate = false
    var isRegUpdate = false
    var isImgUpdate = false
    
    var callback: (()->())?
    var arrayOfVehicleType = [VehicleTypeDataModel]()
    var selectedModel: VehicleTypeDataModel!
    var currentTag = 0
    var topConstraint: CGFloat!
    var base64Img: String!
    var imagePicker = UIImagePickerController()
    let userData = SharedPreference.getUserData()
    var oldPassword: String!
    var imageName = ""
    var isUpdate = false{
        didSet{
            
            
            handelUpdateBtn()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        updateOutlets()
        
        
    }

    func prepareView() {
        hideNavigation()
        callServicForGetVehicleList()
        
        topConstraint = updateBtnTopConstraint.constant
        
        editImgVw.transform = CGAffineTransform(translationX: userImage.frame.width * 0.35, y: userImage.frame.height * 0.35)
        
        phoneNumTextField.setBottomBorder(color: #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1))
        emailTextField.setBottomBorder(color: #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1))
        licenseTextField.setBottomBorder(color: #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1))
        vehicleTypeTextField.setBottomBorder(color: #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1))
        vehicleRegTextField .setBottomBorder(color: #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1))
        passwordTxtFld.setBottomBorder(color: #colorLiteral(red: 0.9907907844, green: 0.7673205137, blue: 0.04130474478, alpha: 1))
        placeHolder1TxtFld.setBottomBorder(color: #colorLiteral(red: 0.9907907844, green: 0.7673205137, blue: 0.04130474478, alpha: 1))
        placeHolder2TxtFld.setBottomBorder(color: #colorLiteral(red: 0.9907907844, green: 0.7673205137, blue: 0.04130474478, alpha: 1))
        updatePassTxtFld.setBottomBorder(color: #colorLiteral(red: 0.9907907844, green: 0.7673205137, blue: 0.04130474478, alpha: 1))
        
        phoneNumTextField.isUserInteractionEnabled = false
        emailTextField.isUserInteractionEnabled = false
        licenseTextField.isUserInteractionEnabled = false
        vehicleTypeTextField.isUserInteractionEnabled = false
        vehicleRegTextField.isUserInteractionEnabled = false
        passwordTxtFld.isUserInteractionEnabled = false
        
        placeHolder1TxtFld.delegate = self
        placeHolder2TxtFld.delegate = self
        
        let status = PHPhotoLibrary.authorizationStatus()
        
        if status == .notDetermined && status == .denied {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .denied{
                    let alert = AppTheme.showAlert("Edit Profile", message:"You are not able to edit Profile pic." , cancelButtonTitle: "OK")
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }

       
        btnUpdate.startColor = UIColor.lightGray
        btnUpdate.endColor = UIColor.lightGray
        btnUpdate.isUserInteractionEnabled = false
        btnUpdate.setTitleColor(UIColor.white, for: .normal)
    }
    
    
    
    func handelUpdateBtn(){
        if isEmailUpdate || isphonUpdate || isLicenUpdate || isRegUpdate || isImgUpdate{
            btnUpdate.startColor = #colorLiteral(red: 0.963699162, green: 0.8433042243, blue: 0.1262178822, alpha: 1)
            btnUpdate.endColor = #colorLiteral(red: 0.963699162, green: 0.762280941, blue: 0.2721533179, alpha: 1)
            btnUpdate.isUserInteractionEnabled = true
            btnUpdate.setTitleColor(UIColor.black, for: .normal)
        }else{
            btnUpdate.startColor = UIColor.lightGray
            btnUpdate.endColor = UIColor.lightGray
            btnUpdate.isUserInteractionEnabled = false
            btnUpdate.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    func updateOutlets() {
        
        //print(userData.image)
        userNameLbl.text = userData.name
        emailTextField.text = userData.email
        phoneNumTextField.text = userData.phone
        vehicleTypeTextField.text = userData.categ_name
        licenseTextField.text = userData.license_num
        vehicleRegTextField.text = userData.truck_reg_num
        passwordTxtFld.text = UserDefaults.standard.value(forKey: "pass") as? String
        oldPassword = UserDefaults.standard.value(forKey: "pass") as? String
        if let userImg = userData.image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if userImg.isEmpty{
                self.userImage.image = #imageLiteral(resourceName: "user_img_default (2)")
            }else{
                self.userImage.kf.setImage(with: URL(string: userImg)!  ,placeholder: #imageLiteral(resourceName: "user_img_default") )
                
            }
        }
    }
    
    // Calling API for update profile
    func callServicForUpdateProfile(param: [String:Any]) {
        
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.UpdateProfile, parameters: param as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                print(data)
                let dataDict = data["data"] as! NSDictionary
                
//                "cate_id" = 5;
//                email = "sandeep.single@gmail.com";
//                image = "";
//                "license_num" = bbh89O;
//                name = Sandeep;
//                phone = 5645785612;
//                "truck_reg_num" = VGH6778;
                
                if let data = dataDict as? [String:Any] {
                    
                    
                    var userDet = self.userData
                    userDet.cate_id = data["cate_id"] as? Int ?? userDet.cate_id
                    userDet.email = data["email"] as? String ?? userDet.email
                    userDet.image = data["image"] as? String ?? userDet.image
                    userDet.license_num = data["license_num"] as? String ?? userDet.license_num
                    
                    userDet.name = data["name"] as? String ?? userDet.name
                    userDet.phone = data["phone"] as? String ?? userDet.phone
                    userDet.truck_reg_num = data["truck_reg_num"] as? String ?? userDet.truck_reg_num
                    
                
//                   var updatedData = UserEntity(withDictionary: data)
//                    updatedData.token = self.userData.token
                    
                    //print(data)
                    
                    SharedPreference.saveUserData(userDet)
                    
                    print(self.userData.carr_id)
                    print(self.userData.truck_reg_num)
                    
                    let alert = AppTheme.showAlert( "Successfully updated", message:"" , cancelButtonTitle: "OK")
                    self.present(alert, animated: true, completion: nil)
                    
                }
                DispatchQueue.main.async {
                    self.prepareView()
                }
            }else {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // Calling API for update profile
    func callServicForGetVehicleList() {
        
        self.startActivityIndicator()
        CommunicationManager().getResponseFor(service: CommunicationManager.WebServiceType.GetTruckCatList, parameters: "") { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                print(data)
                
                if let arrayOfTruckCat = data["data"] as? [[String:Any]] {
                    self.arrayOfVehicleType = [VehicleTypeDataModel]()
                    for item in arrayOfTruckCat {
                        self.arrayOfVehicleType.append(VehicleTypeDataModel.init(withDictionary: item))
                    }
                }
               self.vehicleTypeTableView.reloadData()
            }else if (result == "412") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "Message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
}
// MARK: - Controller Action Methods
extension EditProfileVC {
    
    @IBAction func editUserImgAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func backUpdatePopUp(_ sender : UIButton){
     self.animateOut(popUp: updateView)
    }
    @IBAction func  backAction (_ sender : UIButton) {
      let _ = self.popViewController()
    }
    
    
//    {
//    "new_phone":8305194142,
//    "new_email":"ashish22@gmail.com",
//    "license_num":"13213547468",
//    "categ_id":1074,
//    "truck_reg_num":"44asd2",
//    "profile_image":""
//    }
    
    
    func updatePassword(oldpassword: String, newPassword: String) {
        let param  = [
            "old_pass": oldPassword,
            "new_pass": newPassword
        ]
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.UpdatePassword, parameters: param as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                UserDefaults.standard.set(newPassword, forKey: "pass")
                
                
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
                
            }else if (result == "412") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    
    
    func checkUserUniqueNum(number: String) {
        let param = [ "number": number]
        CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.UniqueNumber, parameters: param as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                
                if "does not exist" == String(describing: data.object(forKey: "message")!){
                    self.phoneNumTextField.text = self.placeHolder2TxtFld.text
                    self.isphonUpdate = true
                    self.isUpdate = true
                }else if "exist" == String(describing: data.object(forKey: "message")!){
                    let alert = AppTheme.showAlert("Number already exists.", message:"" , cancelButtonTitle: "OK")
                    self.present(alert, animated: true, completion: nil)
                }
                
//                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
//                self.present(alert, animated: true, completion: nil)
                
            }else if (result == "412") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func checkUserEmailId(emailId: String) {
        let param = [ "email": emailId]

        CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.UpdateEmail, parameters: param as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                
                if "email does not exist" == String(describing: data.object(forKey: "message")!){
                    self.emailTextField.text  = self.placeHolder1TxtFld.text
                    self.isEmailUpdate = true
                    self.isUpdate = true
                }else if "email already exist" == String(describing: data.object(forKey: "message")!){
                    let alert = AppTheme.showAlert("Email already exists.", message:"" , cancelButtonTitle: "OK")
                    self.present(alert, animated: true, completion: nil)
                }
                
//                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
//                self.present(alert, animated: true, completion: nil)
                
            }else if (result == "412") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    @IBAction func  updateProfileAction (_ sender : UIButton) {
        
        let param = [   "new_phone":phoneNumTextField.text!,
                        "new_email":emailTextField.text!,
                        "license_num": licenseTextField.text!,
                        "categ_id":userData.cate_id,
                        "truck_reg_num": vehicleRegTextField.text!,
                        "profile_image": base64Img,
                        "img_name": imageName
            ] as [String : Any]
        callServicForUpdateProfile(param: param)
    }
    @IBAction func  logoutButtonAction (_ sender : UIButton) {
        // create the alert
        let alert = UIAlertController(title: "Logout", message: "Are you sure,you want to logout?", preferredStyle: .alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "Logout", style: UIAlertAction.Style.default, handler: {
            (UIAlertAction) -> Void in
            
            self.callback?()
            
               self.logout() // logout event call
            
            
            
            
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func  cancelPopUpction (_ sender : UIButton) {
       self.animateOut(popUp: viewTruckType)
    }
    
    @IBAction func  truckTypeButonAction (_ sender : UIButton) {
       self.animateIn(popUp: viewTruckType)
    }
    
    @IBAction func updateFieldAction(_ sender: GradientBtn) {
        
        if placeHolder1TxtFld.text!.isEmpty || placeHolder2TxtFld.text!.isEmpty{
            updateErrorLbl.isHidden = false
            updateErrorLbl.text = "Field can't be empty."
           return
        }
        
        if !confirmUpdatePassVw.isHidden{
            if updatePassTxtFld.text! != placeHolder2TxtFld.text!{
                updateErrorLbl.isHidden = false
                updateErrorLbl.text = "Confirm Password Mismatch."
                return
            }
        }else{
            if placeHolder1TxtFld.text! != placeHolder2TxtFld.text!{
                updateErrorLbl.isHidden = false
                updateErrorLbl.text = "Field Mismatch"
                return
            }
        }
        
        switch currentTag{
        
        case 1:
            if placeHolder1TxtFld.text!.count != 10{
                updateErrorLbl.isHidden = false
                updateErrorLbl.text = "Phone number must be of 10 Digit."
                return
            }
            
            if phoneNumTextField.text != placeHolder2TxtFld.text{
                
                checkUserUniqueNum(number: placeHolder2TxtFld.text!)
                //phoneNumTextField.text = placeHolder2TxtFld.text
            }
        case 2:
            
            if emailTextField.text != placeHolder2TxtFld.text{
                
                checkUserEmailId(emailId: placeHolder2TxtFld.text!)
                //emailTextField.text  = placeHolder1TxtFld.text!
            }
        case 6:
    
//            if placeHolder1TxtFld.text! != UserDefaults.standard.value(forKey: "pass") as! String{
//                updateErrorLbl.isHidden = false
//                updateErrorLbl.text = "Old Password incorrect."
//                return
//            }
//            pass
            //passwordTxtFld.text = placeHolder2TxtFld.text!
            updatePassword(oldpassword: placeHolder1TxtFld.text!, newPassword: placeHolder2TxtFld.text!)
        default:
            print()
        }
        self.animateOut(popUp: updateView)
    }
    
    
    func updatePopupImg(image: UIImage) {
        updatePassImgVw.image = image
        updateImgVw.image = image
        upddateImgVw2.image = image
    }
    
    
    
    @IBAction func editAction(_ sender : UIButton){
        
        currentTag = sender.tag
        updateBtnTopConstraint.constant = topConstraint
        confirmUpdatePassVw.isHidden = true
        
        switch sender.tag {
        
        case 1:
            print("Phone Number")
            updateBtnTopConstraint.constant = 50
            placeHolder1TxtFld.keyboardType = UIKeyboardType.numberPad
            placeHolder2TxtFld.keyboardType = UIKeyboardType.numberPad
            updatePopupImg(image: #imageLiteral(resourceName: "call"))
            setPopUpFields(headerTitle: "UPDATE MOBILE NUMBER", placeHolder1: "ENTER PHONE NO.", placeHolder2: "CONFIRM PHONE NO.")
        case 2:
            print("Email")
            isEmail = true
            updateBtnTopConstraint.constant = 50
            placeHolder1TxtFld.keyboardType = UIKeyboardType.emailAddress
            placeHolder2TxtFld.keyboardType = UIKeyboardType.emailAddress
            updatePopupImg(image: #imageLiteral(resourceName: "mail"))
            setPopUpFields(headerTitle: "UPDATE YOUR EMAIL", placeHolder1: "ENTER YOUR EMAIL", placeHolder2: "CONFIRM YOUR EMAIL")
        case 3:
            print("License")
            licenseTextField.isUserInteractionEnabled = true
            licenseTextField.becomeFirstResponder()
            return
        case 4:
            print("Vechicle Type")
            return
        case 5:
            print("Vechicle Rego")
            vehicleRegTextField.isUserInteractionEnabled = true
            vehicleRegTextField.becomeFirstResponder()
            return
        case 6:
            print("Update Password")
            updatePopupImg(image: #imageLiteral(resourceName: "password"))
            placeHolder1TxtFld.keyboardType = UIKeyboardType.emailAddress
            placeHolder2TxtFld.keyboardType = UIKeyboardType.emailAddress
        
            confirmUpdatePassVw.isHidden = false
            setPopUpFields(headerTitle: "UPDATE PASSWORD", placeHolder1: "Enter current password", placeHolder2: "Enter new password")
        default:
            print("")
        }
        self.animateIn(popUp: updateView)
        placeHolder1TxtFld.becomeFirstResponder()
    }
    
    
    //Update PopUp
    func setPopUpFields(headerTitle: String, placeHolder1: String, placeHolder2: String) {
        headerLbl.text = headerTitle
        placeHolder1TxtFld.text = ""
        placeHolder2TxtFld.text = ""
        placeHolder1TxtFld.placeholder = placeHolder1
        placeHolder2TxtFld.placeholder = placeHolder2
        
    }
    
}

extension EditProfileVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfVehicleType.count == 0 ? 0: arrayOfVehicleType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        let data = self.arrayOfVehicleType[indexPath.row]
        cell.textLabel?.text = data.categ_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        userData.cate_id = arrayOfVehicleType[indexPath.row].id
        vehicleTypeTextField.text = self.arrayOfVehicleType[indexPath.row].categ_name
        self.animateOut(popUp: viewTruckType)
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}
extension EditProfileVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if !updateErrorLbl.isHidden{
            updateErrorLbl.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == licenseTextField{
            if userData.license_num != textField.text{
                isLicenUpdate = true
                isUpdate = true
                
            }else{
                isLicenUpdate = false
                isUpdate = true
            }
        }else if textField.text == vehicleRegTextField.text{
            if userData.truck_reg_num != textField.text{
                isRegUpdate = true
                isUpdate = true
                
            }else{
                
                isRegUpdate = false
                isUpdate = true
            }
        }
    }
}
extension EditProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        let imgData = NSData(data: (info[UIImagePickerController.InfoKey.originalImage] as! UIImage).jpegData(compressionQuality: 1)!)
        var imageSize: Int = imgData.count
        print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)

        if (Double(imageSize) / 1000.0) > 500{
            
            picker.dismiss(animated: true)
            let alert = AppTheme.showAlert("Please upload image smaller than 800kb.", message:"" , cancelButtonTitle: "OK")
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        userImage.image = UIImage(data: image.jpeg(.low)!)
        isImgUpdate = true
        isUpdate = true
        base64Img = toBase64()

        
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset = result.firstObject
            print(asset?.value(forKey: "filename"))
            
            imageName = asset?.value(forKey: "filename") as? String ?? ""
            
        }
        
        
        
//        if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
//            let assetResources = PHAssetResource.assetResources(for: asset)
//
//            imageName = (assetResources.first!.originalFilename)
//        }
        picker.dismiss(animated: true)
    }
    
    func toBase64() -> String? {
        guard let imageData = userImage.image!.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    //MARK:- Download Image
}
extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

class ApiService {
    
 static   func downloadImage(url : String?,completion: @escaping (Data)->()){
        
        
        let request = URLRequest(url: URL(string: url!)!)
        
        let session = URLSession.shared
        
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            
            if error == nil{
                
                if let httpResponse = response as? HTTPURLResponse{
                    
                    switch(httpResponse.statusCode){
                        
                    case 200:
                        if let data = data{
                            completion(data)
                        }
                    default:
                        print("HttpError \(url) \(httpResponse.statusCode)")
                    }
                }
            }else{
                print("Error download data : \(error?.localizedDescription ?? "Error")")
            }
        }
        dataTask.resume()
        
    }
}
