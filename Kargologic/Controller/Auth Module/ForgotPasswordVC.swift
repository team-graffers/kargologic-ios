//
//  ForgotPasswordVC.swift
//  Kargologic
//
//  Created by shrikant upadhyay on 25/08/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var emailtxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func submitAction(_ sender: Any) {
        
        if emailtxt.text!.isEmpty{
            let alert = AppTheme.showAlert("Forgot password", message: "Enter your registered Email.", cancelButtonTitle: "OK")
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let param = ["email" : emailtxt.text!]
        
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.ForgotPass, parameters: param as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                if let dataDict = data as? NSDictionary {
                    
                    
                    print(dataDict)
                    
                    let alert = AppTheme.showAlert(String(describing: data.object(forKey: "data")!), message:"" , cancelButtonTitle: "OK")
                    self.present(alert, animated: true, completion: nil)
                    
                    
//                    DispatchQueue.main.async {
//                      self.backAction(self)
//                    }
                }
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    //patidars0143@gmail.com
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
