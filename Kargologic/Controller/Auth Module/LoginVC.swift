//
//  LoginVC.swift
//  Kargologic
//
//  Created by mac on 27/04/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var phoneNumTextField : UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }
    func prepareView() {
        hideNavigation()
        phoneNumTextField.setBottomBorder(color: #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 1))
        passwordTextField.setBottomBorder(color: #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 1))
//        phoneNumTextField.text = "1245783690"
//        passwordTextField.text = "T5RJf6L1"
    }
    
    // Calling API for LogIn
    func callServiceToLogin(param: [String: AnyObject]){
        
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.Login, parameters: param as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                if let dataDict = data as? NSDictionary {
                    print(dataDict)
//                    if let token = dataDict.value(forKey: "token") as? String {
//                        SharedPreference.storeAuthToken("Token "+token)
//                    }
                    if let data = dataDict as? [String:Any] {
                        
                        print(data)
                        
                        let userData = UserEntity(withDictionary: data)
                        
                        UserDefaults.standard.set(self.passwordTextField.text!, forKey: "pass")
                        SharedPreference.saveUserData(userData)
                    }
                }
                self.goToHomePage()
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}
// MARK: - Controller Action Methods
extension LoginVC {
    
    @IBAction func  loginAction (_ sender : UIButton) {
        
//           goToHomePage()
        
        var strError = String()
        if phoneNumTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            strError = ErrorMesssage.phoneNum
        }else if passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            strError = ErrorMesssage.password
        }
//        else  if tfPwd.text!.count < 6 {
//            strError = ErrorMesssage.passwordLength
//        }

        if strError.isEmpty {
        let data = ["phone": phoneNumTextField.text!,
                    "password":passwordTextField.text!]
            
             callServiceToLogin(param: data as [String : AnyObject])
        }else{
            let alert = AppTheme.showAlert(strError, message:"" , cancelButtonTitle: "OK")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
   @IBAction func  forgotPass (_ sender : UIButton) {
    
    let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC")
    
    self.present(vc! , animated: true, completion: nil)
    
    }
    @IBAction func  privacyButtonAction (_ sender : UIButton) {
        openURL(with: "https://kargologic.com/privacy-policy" )
    }
}
