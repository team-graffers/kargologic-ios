//
//  UpcomingVC.swift
//  Kargologic
//
//  Created by mac on 28/04/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class UpcomingVC: UIViewController {

    @IBOutlet weak var editProfBtn: UIButton!
    @IBOutlet weak var upcomingTableView: UITableView!
    @IBOutlet weak var noDataLabel: UIView!
    var arrayOfUpcomingList = [HomeDataModel]()
    var refresher:UIRefreshControl!
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       //prepareView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        let userData = SharedPreference.getUserData()

        if let userImg = userData.image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if userImg.isEmpty{
                editProfBtn.setImage(#imageLiteral(resourceName: "user_img_default (2)"), for: .normal)
            }else{
                                let imageView =  UIImageView()
                
                                print(userImg)
                                imageView.kf.setImage(with: URL(string: userImg)!  ,placeholder: #imageLiteral(resourceName: "user_img_default"))
                                editProfBtn.setImage( imageView.image, for: .normal)
                
                
//                ApiService.downloadImage(url: userImg) { (data) in
//                    DispatchQueue.main.async {
//                        self.editProfBtn.setImage( UIImage(data: data), for: .normal)
//                    }
//                }
                
            }
        }
    }
    
    func prepareView() {
        hideNavigation()
        addPullToRefresh()
        callServiceForUpcomingData(status: JobStatus.upcoming, isPullToRefresh : false)
    }
    func addPullToRefresh(){
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1)
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.upcomingTableView!.refreshControl = refresher
    }
    @objc func loadData() {
        //code to execute during refresher
        
        callServiceForUpcomingData(status: JobStatus.upcoming, isPullToRefresh : true)
        
        stopRefresher()         //Call this to stop refresher
    }
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    func callServiceForUpcomingData(status: String,isPullToRefresh : Bool) {
        if !isPullToRefresh{ self.startActivityIndicator()}
        CommunicationManager().getResponseFor(service: CommunicationManager.WebServiceType.Dashboard, parameters: status) { (result, data) in
            if !isPullToRefresh{ self.stopActivityIndicator()}

            if (result == "200") {
                if let inProgressDataArray = data["data"] as? [[String:Any]] {
                    print(inProgressDataArray)
                    self.arrayOfUpcomingList = [HomeDataModel]()
                    for item in inProgressDataArray {
                        self.arrayOfUpcomingList.append(HomeDataModel.init(withDictionary: item))
                    }
                    if self.arrayOfUpcomingList.isEmpty {
                        self.noDataLabel.isHidden = false
                    }else{
                        self.noDataLabel.isHidden = true
                    }
                }
                self.upcomingTableView.reloadData()
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

// MARK: - Controller Action
extension UpcomingVC {

    
    @IBAction func  editProfileAction (_ sender : UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func  notificationAction (_ sender : UIButton) {
        
    }
}
// MARK: - View Controller DataSource and Delegate
extension UpcomingVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayOfUpcomingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.upcoming, for: indexPath) as! AllTabsTableViewCell
        
        let data = arrayOfUpcomingList[indexPath.row]
        
        cell.jobNoLabel.text = "Job No \(data.extraDetailsDictionary.job_no)"
        cell.companyNameLabel.text = data.extraDetailsDictionary.customer_company_name
        cell.shortNameLabel.text = data.extraDetailsDictionary.customer_company_name.getShorNameFromString()
        if !data.pickUpArray.isEmpty {
            cell.pickUpLocationLabel.text = data.pickUpArray[0].address
            cell.pickUpDateLabel.text = formatter.date(from: data.pickUpArray[0].pickup_date_time)?.date
        }else{
            cell.pickUpLocationLabel.text = "NA"
            cell.pickUpDateLabel.text = "NA"
        }
        if !data.deliveryArray.isEmpty {
            cell.deliveryLocationLabel.text = data.deliveryArray[0].address
            cell.deliveryDateLabel.text = formatter.date(from: data.deliveryArray[0].delivery_date_time)?.date
        }else{
            cell.deliveryLocationLabel.text = "NA"
            cell.deliveryDateLabel.text = "NA"
        }
        cell.ViewMoreButtonClick = {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewMoreVC") as! ViewMoreVC
            vc.hidesBottomBarWhenPushed = true
            vc.viewMoreData = data
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        cell.CallButtonClick = {
            
            if  data.extraDetailsDictionary.carrier_number != nil{
                
                let url:NSURL = NSURL(string: "tel://\(data.extraDetailsDictionary.carrier_number)") ?? NSURL(string: "0")!
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.open(url as URL, options: [ : ], completionHandler: nil)
                }else{
                    let alert = AppTheme.showAlert("Number not available." , message:"" , cancelButtonTitle: "OK")
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let alert = AppTheme.showAlert("Number not available." , message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
            
           
        }
        return cell
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //   return menuItem[indexPath.section].collapsed! ? 0 : 44.0
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TravellerHomeVC") as! HomeVC
        //                self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}
