//
//  InProgressVC.swift
//  Kargologic
//
//  Created by mac on 28/04/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit
import CoreLocation

// UITableViewCell for In Progress!
class InProgressCell : UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
}
class InProgressVC: UIViewController {
    
    @IBOutlet weak var notificationBtn: UIButton!
    
    @IBOutlet weak var bellIndicatorVw: UIView!{
        didSet{
            self.bellIndicatorVw.layer.cornerRadius = self.bellIndicatorVw.frame.height * 0.5
        }
    }
    static let shared = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InProgressVC") as! InProgressVC
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }()
    
    @IBOutlet weak var noDataLabel: UIView!
    @IBOutlet weak var editProfBtn: UIButton!
    
    @IBOutlet weak var jobSearchBar: UISearchBar!
    @IBOutlet weak var inProgressTableView: UITableView!
    
    var locationManager:CLLocationManager! = nil
    var notificaitonList = [NotificationModel]()
    var timer: Timer?
    typealias completionHanlder = (_ lat: String, _ long: String) -> Void
    var completion: completionHanlder! = nil
    
    var arrayOfInProgressList = [HomeDataModel]()
    var refresher:UIRefreshControl!
    
    var previousCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setUptimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(runTimeCode), userInfo: nil, repeats: true)

    }
  
    var notificstionList = [NotificationModel]()
    @objc func runTimeCode(){
                    print("Timer is Running")
                    self.etaTimeCalculator()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        prepareView()
        setUptimer()
    }
    
    var prevURL = ""
    override func viewDidAppear(_ animated: Bool) {
        
        
        let userDefault = UserDefaults.standard
        if userDefault.value(forKey: "counter") != nil{
            previousCount = userDefault.value(forKey: "counter") as! Int
        }
        
        let userData = SharedPreference.getUserData()

        if let userImg = userData.image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            if userImg.isEmpty{
                editProfBtn.setImage(#imageLiteral(resourceName: "user_img_default (2)"), for: .normal)
            }else{

                let imageView =  UIImageView()
                
                print(userImg)
                imageView.kf.setImage(with: URL(string: userImg)!  ,placeholder: #imageLiteral(resourceName: "user_img_default"))
                editProfBtn.setImage( imageView.image, for: .normal)
                
//                ApiService.downloadImage(url: userImg) { (data) in
//                    DispatchQueue.main.async {
//                        self.editProfBtn.setImage( UIImage(data: data), for: .normal)
//                    }
//                }
                
            }
        }
        
        handelNotificatiob()
    }
    
    func handelNotificatiob(){
        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(functionOne), userInfo: nil, repeats: true)
    }
    
    @objc func functionOne()  {
        getNotificationList(status: "")
    }
    
    
    func getNotificationList(status: String) {
        
        CommunicationManager().getResponseFor(service: CommunicationManager.WebServiceType.GetNotification, parameters: status) { (result, data) in
            if (result == "200") {
                if let noticationData = data["data"] as? [[String:Any]] {
                    if self.previousCount != noticationData.count{
                        self.notificstionList.removeAll()
                        self.bellIndicatorVw.isHidden = false
                        for item in noticationData{
                            self.notificstionList.append(NotificationModel(id: item["id"] as? Int ?? 0,
                                                                           orderStatus: item["order_status"] as? String ?? "",
                                                                           comments: item["comments"] as? String ?? "",
                                                                           isRead: item["is_read"] as? Bool ?? false,
                                                                           dateTimeOfCreate: item["date_time_of_create"] as? String ?? "",
                                                                           isActive: item["is_active"] as? Bool ?? true,
                                                                           orderID: item["OrderID"] as? Int ,
                                                                           createdByCarrier: item["created_by_carrier"] as? Int ?? 0,
                                                                           createdByCustomer: item["created_by_customer"] as? String ?? "",
                                                                           createdByDriver: item["created_by_driver"] as? String ?? "",
                                                                           createdForCarrier: item["created_for_carrier"] as? String ?? "",
                                                                           createdForCustomer: item["created_for_customer"] as? String ?? "",
                                                                           createdForDriver: item["created_for_driver"] as? Int ?? 0))
                        }
                        
                        
                    }else{
                        self.bellIndicatorVw.isHidden = true
                    }
                }
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    func prepareView() {
        
//        if let userImg = userData.image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
//            if userImg.isEmpty{
//                editProfBtn.setImage(#imageLiteral(resourceName: "user_img_default (2)"), for: .normal)
//            }else{
//                let imageView =  UIImageView()
//                imageView.kf.setImage(with: URL(string: userImg)!  ,placeholder: #imageLiteral(resourceName: "user_img_default"))
//                editProfBtn.setImage( imageView.image, for: .normal)
//            }
//        }
        hideNavigation()
        addPullToRefresh()
        callServiceForInProgressData(status: JobStatus.inProgress, isPullToRefresh : false)
    }

    func addPullToRefresh(){
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1)
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.inProgressTableView!.refreshControl = refresher
    }
    @objc func loadData() {
        //code to execute during refresher
        
        callServiceForInProgressData(status: JobStatus.inProgress, isPullToRefresh : true)
        
        stopRefresher()         //Call this to stop refresher
    }
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    func callServiceForInProgressData(status: String,isPullToRefresh : Bool) {
        if !isPullToRefresh{ self.startActivityIndicator()}
        CommunicationManager().getResponseFor(service: CommunicationManager.WebServiceType.Dashboard, parameters: status) { (result, data) in
            if !isPullToRefresh{ self.stopActivityIndicator()}
            
            if (result == "200") {
                if let inProgressDataArray = data["data"] as? [[String:Any]] {
                    print(inProgressDataArray)
                    self.arrayOfInProgressList = [HomeDataModel]()
                    for item in inProgressDataArray {
                        self.arrayOfInProgressList.append(HomeDataModel.init(withDictionary: item))
                    }
                    if self.arrayOfInProgressList.isEmpty {
                        self.noDataLabel.isHidden = false
                    }else{
                        self.noDataLabel.isHidden = true
                    }
                }
                self.timer?.fire()
                //self.etaTimeCalculator()
                self.inProgressTableView.reloadData()
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
}
// MARK: - Controller Action
extension InProgressVC {
    
   
//    @IBAction func  callAction (_ sender : UIButton) {
//        let index = sender.tag - 200;let indexSection = Int(sender.accessibilityIdentifier!)
//        let dataRow = arrAppointmentsSection[indexSection!]["data"] as! Array<Dictionary<String, Any>>
//        let dict = dataRow[index]
//        let str = (dict["mobile_no"] as! String)
//        if sender.titleLabel?.text != "NA" {
//            let url:NSURL = NSURL(string: "tel://\(str)")!
//            if UIApplication.shared.canOpenURL(url as URL) {
//                UIApplication.shared.open(url as URL, options: [ : ], completionHandler: nil)
//            }
//        }
//    }
//    @IBAction func  getDirectionAction (_ sender : UIButton) {
//        let index = sender.tag - 300;let indexSection = Int(sender.accessibilityIdentifier!)
//        let dataRow = arrAppointmentsSection[indexSection!]["data"] as! Array<Dictionary<String, Any>>
//        let dict = dataRow[index]
//        //        var lat1 = Float(); var long1 = Float()
//        //        if let lat = dict["stylemee_user_latitude"] as? Float {
//        //            lat1 = lat
//        //        }else{
//        //            lat1 = 0.0
//        //        }
//        //        if let long = dict["stylemee_user_longitude"] as? Float {
//        //            long1 = long
//        //        }else{
//        //            long1 = 0.0
//        //        }
//        let add =  dict["address"] as! String
//        openMapForPlace(address: add)
//
//        //        let url = "http://maps.apple.com/maps?saddr=\(22),\(75)&daddr=\(23),\(75)"
//        //        UIApplication.shared.openURL(URL(string:url)!)
//    }
    
    @IBAction func  editProfileAction (_ sender : UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func  notificationAction (_ sender : UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NoticationViewController") as! NoticationViewController
        vc.notificstionList = notificstionList
        vc.updateCounter = { counter in
            self.previousCount = counter
            
            let userDefault = UserDefaults.standard
            userDefault.set(counter, forKey: "counter")
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: - View Controller DataSource and Delegate
extension InProgressVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayOfInProgressList.count == 0 {
            return 0
        }else {
            return arrayOfInProgressList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.inProgress, for: indexPath) as! AllTabsTableViewCell
        
        let data = arrayOfInProgressList[indexPath.row]
        
        cell.jobNoLabel.text = "Job No \(data.extraDetailsDictionary.job_no)"
        cell.companyNameLabel.text = data.extraDetailsDictionary.customer_company_name
        cell.shortNameLabel.text = data.extraDetailsDictionary.customer_company_name.getShorNameFromString()
        
        if data.etaDelivery == ErrorMesssage.etaErrMsg{
         cell.deliveryTimeLabel.textColor  = UIColor.red
        }else{
            cell.deliveryTimeLabel.textColor = #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1)
        }
        
        if data.etaPickUp == ErrorMesssage.etaErrMsg{
            cell.pickUpTimeLabel.textColor  = UIColor.red
        }else{
            cell.pickUpTimeLabel.textColor = #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1)
        }
        
        cell.deliveryTimeLabel.text = data.etaDelivery
        cell.pickUpTimeLabel.text = data.etaPickUp
        
        if !data.pickUpArray.isEmpty {
            cell.pickUpLocationLabel.text = data.pickUpArray[0].address
            cell.pickUpDateLabel.text = formatter.date(from: data.pickUpArray[0].pickup_date_time)?.date
        }else{
            cell.pickUpLocationLabel.text = "NA"
            cell.pickUpDateLabel.text = "NA"
        }
        if !data.deliveryArray.isEmpty {
          cell.deliveryLocationLabel.text = data.deliveryArray[0].address
            cell.deliveryDateLabel.text = formatter.date(from: data.deliveryArray.last!.delivery_date_time)?.date
        }else{
            cell.deliveryLocationLabel.text = "NA"
            cell.deliveryDateLabel.text = "NA"
        }
       
        cell.ViewMoreButtonClick = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewMoreVC") as! ViewMoreVC
            vc.hidesBottomBarWhenPushed = true
            vc.viewMoreData = data
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell.CallButtonClick = {
            
            
            
            if  data.extraDetailsDictionary.carrier_number != nil{
                
                let url:NSURL = NSURL(string: "tel://\(data.extraDetailsDictionary.carrier_number)") ?? NSURL(string: "0")!
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.open(url as URL, options: [ : ], completionHandler: nil)
                }else{
                    let alert = AppTheme.showAlert("Number not available." , message:"" , cancelButtonTitle: "OK")
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let alert = AppTheme.showAlert("Number not available." , message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
            
            
            
            
            
            
//            let url:NSURL = NSURL(string: "tel://\(data.extraDetailsDictionary.carrier_number)")!
//            if UIApplication.shared.canOpenURL(url as URL) {
//                UIApplication.shared.open(url as URL, options: [ : ], completionHandler: nil)
//            }
        }
            return cell
            
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //   return menuItem[indexPath.section].collapsed! ? 0 : 44.0
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TravellerHomeVC") as! HomeVC
//                self.navigationController?.pushViewController(vc, animated: true)
      
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
      
    }
}


extension InProgressVC{
    
    func function_GetCurrentLocation(location:@escaping (String,String)->Void) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.startUpdatingLocation()
            self.completion =  { (lat, lng) in
                location(lat,lng)
            }
        }
    }
    
    func etaTimeCalculator(){
        
        
        for arrItem in arrayOfInProgressList{
            
            for pickUpItem in arrItem.pickUpArray{
                if pickUpItem.actual_pickup_date_time != ""{
                    arrItem.etaPickUp = "Completed"
                }
            }
            
            for pickUpItem in arrItem.deliveryArray{
                if pickUpItem.actual_delivery_date_time != ""{
                    arrItem.etaDelivery = "Completed"
                }
            }
            
        }
        
        
        function_GetCurrentLocation { (lat, long) in
            
            for arrItem in self.arrayOfInProgressList{
                var locations = [Any]()
                //Merging Both Array with removal of Completed Process
                
                var isCompleted = false
                
                for item in arrItem.pickUpArray{
                    print(item.actual_pickup_date_time)
                    
                    if item.actual_pickup_date_time == ""{
                        locations.append(item)
                    }else{
                        isCompleted = true
                    }
                }
                for item in arrItem.deliveryArray{
                    print(item.actual_delivery_date_time)
                    if item.actual_delivery_date_time == ""{
                        locations.append(item)
                    }
                }
                
                
                if !locations.isEmpty{
                    //Sort
                    locations.sort { (obj1, obj2) -> Bool in
                        var date1 = ""
                        var date2 = ""
                        //Check First Object Type
                        if let obj = obj1 as? PickUp{
                            date1 = obj.pickup_date_time_to
                        }
                        if let obj = obj1 as? Delivery{
                            date1 = obj.delivery_date_time_to
                        }
                        //Check second object Type
                        if let obj = obj2 as? PickUp{
                            date2 = obj.pickup_date_time_to
                        }
                        if let obj = obj1 as? Delivery{
                            date2 = obj.delivery_date_time_to
                        }
                        return date1 < date2
                    }
                    if !isCompleted{
                        var wayPointStr1 = "waypoint0=geo!\(lat),\(long)&"
                        let loc = locations.first
                        if let pt1 = loc as? PickUp{
                            wayPointStr1 += "waypoint1=geo!" + pt1.latitude + "," + pt1.longitude + "&"
                        }
                        self.getPickETA(wayPointStr: wayPointStr1, model: arrItem)
                    }else{
                        arrItem.etaPickUp = "Completed"
                    }
                    
                    
                    var wayPointStr = "waypoint0=geo!\(lat),\(long)&"
                    
                    for location in 0...locations.count - 1 {
                        var wayPoint = ""
                        if let pt1 = locations[location] as? PickUp{
                            wayPoint += pt1.latitude + "," + pt1.longitude
                        }
                        if let pt1 = locations[location] as? Delivery{
                            wayPoint += pt1.latitude + "," + pt1.longitude
                        }
                        wayPointStr += "waypoint\(location + 1)=geo!\(wayPoint)&"
                        
                        self.getDropETA(wayPointStr: wayPointStr, model: arrItem)
                    }
                }
                
            }
        }
    }
    
    
    func getPickETA(wayPointStr: String, model: HomeDataModel) {
        
        if AppTheme.isConnectedToNetwork() {
            var serviceStr = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&\(wayPointStr)mode=fastest;truck;traffic:disabled"
            serviceStr = serviceStr.replacingOccurrences(of: " ", with: "%20")
            
            let request = NSMutableURLRequest(url: URL(string: serviceStr)!)
            request.httpMethod = "GET"
            CommunicationManager().callWebservice(request as URLRequest?) { (responseData, responseCode) in
                DispatchQueue.main.async(execute: {
                    
                    if responseData is NSDictionary {
                        if responseCode == "200" {
                            let response = ((responseData["response"] as! [String: Any])["route"] as! [Any]).first
                            var totalTime = (((response as! [String: Any])["summary"] as! [String: Any])["trafficTime"]) as? Double ?? 00
                            
                            let days = Int(totalTime / (24*3600))
                            let hour = ((totalTime.truncatingRemainder(dividingBy:(24*3600)))/3600)
                            let min = Int((totalTime.truncatingRemainder(dividingBy:(3600*24))).truncatingRemainder(dividingBy: 3600)/60)
                            model.etaPickUp = "\(days)D \(Int(hour))H \(min)M"
                            self.inProgressTableView.reloadData()
                            // completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            //Handel Over here long distance
                            print(responseData)
                            //completion("400", responseData as AnyObject)
                        }
                    }else {
                        // completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            //completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
        
    }
    
    func getDropETA(wayPointStr: String, model: HomeDataModel){
        if AppTheme.isConnectedToNetwork() {
            var serviceStr = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&\(wayPointStr)mode=fastest;truck;traffic:disabled"
            serviceStr = serviceStr.replacingOccurrences(of: " ", with: "%20")
            
            let request = NSMutableURLRequest(url: URL(string: serviceStr)!)
            request.httpMethod = "GET"
            CommunicationManager().callWebservice(request as URLRequest?) { (responseData, responseCode) in
                DispatchQueue.main.async(execute: {
                    
                    if responseData is NSDictionary {
                        if responseCode == "200" {
                            let response = ((responseData["response"] as! [String: Any])["route"] as! [Any]).first
                            var totalTime = (((response as! [String: Any])["summary"] as! [String: Any])["trafficTime"]) as? Double ?? 00
                          
                            let days = Int(totalTime / (24*3600))
                            let hour = ((totalTime.truncatingRemainder(dividingBy:(24*3600)))/3600)
                            let min = Int((totalTime.truncatingRemainder(dividingBy:(3600*24))).truncatingRemainder(dividingBy: 3600)/60)
                            model.etaDelivery = "\(days)D \(Int(hour))H \(min)M"
                            
                            print(totalTime)
                            
                            self.inProgressTableView.reloadData()
                            // completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            //Handel Over here long distance
                            print(responseData)
                            //completion("400", responseData as AnyObject)
                        }
                    }else {
                        // completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            //completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
}

extension InProgressVC: CLLocationManagerDelegate{
    //Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
            if let isLocation = locations.first {
                self.completion("\(isLocation.coordinate.latitude)","\(isLocation.coordinate.longitude)")
                          self.locationManager.stopUpdatingLocation()
                            locationManager.delegate = nil
                            locationManager = nil
            }
            
            
    }
}

