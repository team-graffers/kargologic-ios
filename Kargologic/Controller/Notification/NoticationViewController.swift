//
//  NoticationViewController.swift
//  Kargologic
//
//  Created by shrikant upadhyay on 10/10/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit

class NoticationViewController: UIViewController {
    
    var notificstionList = [NotificationModel]()
    var currentCounter = -1
    var updateCounter :((_ counter: Int)->())?
    
    @IBOutlet weak var notificaitonTblVw: UITableView!{
        didSet{
            self.notificaitonTblVw.delegate = self
            self.notificaitonTblVw.dataSource = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        currentCounter = notificstionList.count
        
        updateCounter?(currentCounter)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NoticationViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificstionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notification_cell") as! NotificationTableViewCell
        cell.model = notificstionList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewMoreVC") as! ViewMoreVC
        vc.hidesBottomBarWhenPushed = true
        vc.orderId = notificstionList[indexPath.row].orderID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateTImeLbl: UILabel!
    @IBOutlet weak var shortNameLbl: UILabel!
    var model: NotificationModel!{
        didSet{
            titleLbl.text = self.model.comments
            shortNameLbl.text = self.model.comments!.getShorNameFromString()
            dateTImeLbl.text = formatDate(dateStr: self.model.dateTimeOfCreate!)
        }
    }
    
    func formatDate(dateStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z" // This formate is input formated .
        
        let dateFormater2 = DateFormatter()
        dateFormater2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z"
        
        let formateDate = dateFormatter.date(from:dateStr) ?? dateFormater2.date(from: dateStr)
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm a"
        
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        
        return dateFormatter.string(from: formateDate!)
    }
    
    @IBAction func notificationAction(_ sender: Any) {
    }
}
