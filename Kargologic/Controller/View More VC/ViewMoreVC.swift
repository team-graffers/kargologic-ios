//
//  ViewMoreVC.swift
//  Kargologic
//
//  Created by sachin jain on 27/05/19.
//  Copyright © 2019 GrafferSid. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation

class AssectTableViewCell: UITableViewCell{
    @IBOutlet weak var assetIdLbl: UILabel!
    @IBOutlet weak var assetTypeLbl: UILabel!
}

class ViewMoreVC: UIViewController {
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shortNameLabel: UILabel!
    @IBOutlet weak var orderStatusDotColor: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var orderStatusImage: UIImageView!
    @IBOutlet weak var jobNoLabel: UILabel!
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var deliveryLocationLabel: UILabel!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var deliveryDateLabel: UILabel!
    @IBOutlet weak var additionalNoteLabel: UILabel!
    
    @IBOutlet weak var orderStatusTableView: UITableView!
     @IBOutlet weak var orderStatusViewHeight: NSLayoutConstraint!
    @IBOutlet weak var attachementsTableView: UITableView!
    @IBOutlet weak var attachementsTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet var assetView: UIView!
    @IBOutlet weak var assetTblVw: UITableView!
    
    var orderId: Int!
    var currentAssetList = [Asset]()
    var viewMoreData = HomeDataModel()
    var arrayOfViewMoreData = [HomeDataModel]()
    var orderStatusDataArray = [PickUpAndDelivery]()
    var orderCommentDataArray = [OrderCommentDataModel](){
        didSet{
           commentTableView.isHidden = self.orderCommentDataArray.isEmpty ? true : false
        }
    }
    var attachmentDataArray = [AttachmentDataModel](){
        didSet{
            attachementsTableView.isHidden = self.attachmentDataArray.isEmpty ? true : false
        }
    }
    var enrouteDate: String!
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }()
    var isButtonVisible = false
    
    var index_path = 0
    var indexArray = [Int]()
    
    
    var recentETA = ""{
        didSet{
            orderStatusTableView.reloadData()
        }
    }
    var index = -1
    var timer: Timer?
    
    var locationManager:CLLocationManager! = nil
    typealias completionHanlder = (_ lat: String, _ long: String) -> Void
    var completion: completionHanlder! = nil
    
     @IBOutlet weak var viewImage: UIView!
     @IBOutlet weak var ImagePod: UIImageView!
    
    // Comment outlets
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var commentTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var commentTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         prepareView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        timer?.invalidate()
        setUptimer()
    }
    
    func prepareView() {
        hideNavigation()
        updateAllOutlets()
        commentTextField.setBottomBorder(color: #colorLiteral(red: 0.960396111, green: 0.7444809675, blue: 0, alpha: 1) )
        callServiceForViewMoreData(orderID: "\(viewMoreData.extraDetailsDictionary.order_id)")
    }
    func updateAllOutlets() {
        let data = viewMoreData.extraDetailsDictionary
        
        if data.status == JobStatus.inProgress {
           orderStatusImage.image = #imageLiteral(resourceName: "inprogress")
            orderStatusDotColor.backgroundColor = #colorLiteral(red: 0.3215686275, green: 0.737254902, blue: 1, alpha: 1)
            orderStatusLabel.text = "In-Progress"
//            commentButton.isHidden = false
        }else if data.status == JobStatus.upcoming {
            orderStatusImage.image = #imageLiteral(resourceName: "upcoming")
            orderStatusDotColor.backgroundColor = #colorLiteral(red: 0.5882352941, green: 0.05882352941, blue: 0.02745098039, alpha: 1)
            orderStatusLabel.text = "Pending"
//            commentButton.isHidden = true
        }else if data.status == JobStatus.completed {
            orderStatusImage.image = #imageLiteral(resourceName: "compelete")
            orderStatusDotColor.backgroundColor = #colorLiteral(red: 0.2249015868, green: 0.9887072444, blue: 0.8931125998, alpha: 1)
            orderStatusLabel.text = "Completed"
//            commentButton.isHidden = false
        }
        jobNoLabel.text = "Job Number #\(data.job_no)"
        companyNameLabel.text = data.customer_company_name
        shortNameLabel.text = data.customer_company_name.getShorNameFromString()
        //orderStatusLabel.text = data.status
        pickUpLocationLabel.text = viewMoreData.pickUpArray[0].address
        pickUpDateLabel.text = formatter.date(from: viewMoreData.pickUpArray[0].pickup_date_time)?.date
        deliveryLocationLabel.text = viewMoreData.deliveryArray.last!.address
        deliveryDateLabel.text = formatter.date(from: viewMoreData.deliveryArray.last!.delivery_date_time)?.date
        
        additionalNoteLabel.text = data.additional_note.isEmpty ? "No additional note available" : data.additional_note
    }
    
    func callServiceForViewMoreData(orderID: String) {
       self.startActivityIndicator()
        CommunicationManager().getResponseFor(service: CommunicationManager.WebServiceType.ViewMore, parameters: orderID) { (result, data) in
           self.stopActivityIndicator()
            
            if (result == "200") {
                 if let viewMoreDataArray = data["main"] as? [[String:Any]] {
                    print(viewMoreDataArray)
                    self.orderStatusDataArray = [PickUpAndDelivery]()
                    self.indexArray = [Int]()
                    self.isButtonVisible = false
                    for item in viewMoreDataArray {
                        self.arrayOfViewMoreData.append(HomeDataModel.init(withDictionary: item))
                    }
                    self.viewMoreData = self.arrayOfViewMoreData[0]
                    self.updateAllOutlets()
                
         //           self.orderStatusDataArray = self.viewMoreData.pickUpArrayAndDeliveryArray.sorted(by: { $0.timesTamp < $1.timesTamp})
                    
                    
                    self.orderStatusDataArray = self.viewMoreData.pickUpArrayAndDeliveryArray.sorted(by: { (ob1, ob2) -> Bool in

                    var ob1Str = ""
                        if ob1.data_of == OrderType.pickUp{
                            ob1Str = ob1.pickup_date_time_to
                        }else{
                            ob1Str = ob1.delivery_date_time_to
                        }

                       var ob2Str = ""
                        if ob2.data_of == OrderType.pickUp{
                            ob2Str = ob2.pickup_date_time_to
                        }else{
                            ob2Str = ob2.delivery_date_time_to
                        }
                        return ob1Str < ob2Str
                    })

                    self.orderStatusTableView.reloadData()
                    
                    self.enrouteDate = viewMoreDataArray.first?["order_enroute"] as? String ?? ""
                }
                if let oderCommentArray = data["order_comment"] as? [[String:Any]] {
                    self.orderCommentDataArray = [OrderCommentDataModel]()
                    for item in oderCommentArray {
                        self.orderCommentDataArray.append(OrderCommentDataModel.init(withDictionary: item))
                    }
                    self.commentTableView.reloadData()
                }
                
                if let attachmentArray = data["attachment"] as? [[String:Any]] {
                    self.attachmentDataArray = [AttachmentDataModel]()
                    for item in attachmentArray {
                        self.attachmentDataArray.append(AttachmentDataModel.init(withDictionary: item))
                    }
                    //self.etaCalculator()
                    self.timer?.fire()
                    self.attachementsTableView.reloadData()
                }
                
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    fileprivate func updateStatus() {
        //self.orderStatusTableView.reloadData()
        
        index = -1
        recentETA = ""
        self.arrayOfViewMoreData.removeAll()
        self.orderStatusDataArray.removeAll()
        self.orderCommentDataArray.removeAll()
        self.attachmentDataArray.removeAll()
        self.viewDidLoad()
    }
    
    func callServiceForUpdateOrderStatus(status: String,paramData : [String:Any]) {
        print("Sending PARAM => \(paramData)")
         self.startActivityIndicator()
        CommunicationManager().getResponseForQueryParam(service: CommunicationManager.WebServiceType.ChangeOrderStatus, urlParameters: status, reqParameters: paramData as NSDictionary) { (result, data) in
             self.stopActivityIndicator()
            
            if (result == "200") {
                print(result)
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    self.callServiceForViewMoreData(orderID: "\(self.viewMoreData.extraDetailsDictionary.order_id)")
                    self.updateStatus()
                }
                
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    
    func setUptimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(runTimeCode), userInfo: nil, repeats: true)
        
    }
    @objc func runTimeCode(){
        print("Timer is Running")
        self.etaCalculator()
    }
    
    func callServiceForUpdatePickupDropActualTime(serviceType: CommunicationManager.WebServiceType!,paramData : [String:Any]) {
        
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(service: serviceType, parameters: paramData as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                if self.index_path == self.orderStatusDataArray.count {
                    let param = [
                        "order_id": self.viewMoreData.extraDetailsDictionary.order_id,
                        "carr_id":SharedPreference.getUserData().carr_id,
                        "en_route_time": nil,
                        "final_delivery_date_time":Date().formatted(format: "yyyy-MM-dd'T'HH:mm:ss'Z'")
                        ] as [String : Any?]
                    self.callServiceForUpdateOrderStatus(status: "pod", paramData: param as [String : Any])
                }else {
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    self.callServiceForViewMoreData(orderID: "\(self.viewMoreData.extraDetailsDictionary.order_id)")
                    self.updateStatus()
                }
             }
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    // MARK: - ImagePickerController
    func openCamera () {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        AppTheme.requestForAccessCamera { (accessGranted) -> Void in
            if accessGranted {
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    imagePickerController.sourceType = .camera
                    self.present(imagePickerController, animated: true, completion: {})
                } else {
                    let alert : UIAlertController = AppTheme.showAlert("Alert", message: "Camera Not Available", cancelButtonTitle: "DONE")
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                let alert =  AppTheme.alertToEncourageCameraAccessInitially()
                self.present(alert , animated: true, completion: nil)
            }
        }
    }
    @IBAction func removeAssetAction(_ sender: Any) {
        animateOut(popUp: assetView)
    }
    // Calling API for send comment
    func callServiceTosendComment(param: [String: AnyObject]){
        
        self.startActivityIndicator()
        CommunicationManager().getResponseForPost(service: CommunicationManager.WebServiceType.CommentSend, parameters: param as NSDictionary) { (result, data) in
            self.stopActivityIndicator()
            
            if (result == "200") {
                self.commentTextField.text = ""
               self.callServiceForViewMoreData(orderID: "\(self.viewMoreData.extraDetailsDictionary.order_id)")
                
            }else if (result == "400") {
                let alert = AppTheme.showAlert(String(describing: data.object(forKey: "message")!), message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
// MARK: - Controller Action Methods
extension ViewMoreVC {
    
    @IBAction func  backAction (_ sender : UIButton) {
        let _ = self.popViewController()
    }
    @IBAction func  callButtonAction (_ sender : UIButton) {
        
        
//        if  data.extraDetailsDictionary.carrier_number != nil{
//
//            let url:NSURL = NSURL(string: "tel://\(data.extraDetailsDictionary.carrier_number)") ?? NSURL(string: "0")!
//            if UIApplication.shared.canOpenURL(url as URL) {
//                UIApplication.shared.open(url as URL, options: [ : ], completionHandler: nil)
//            }else{
//                let alert = AppTheme.showAlert("Number not available." , message:"" , cancelButtonTitle: "OK")
//                self.present(alert, animated: true, completion: nil)
//            }
//        }
        
        
        if viewMoreData.extraDetailsDictionary.carrier_number != nil{
            let url:NSURL = NSURL(string: "tel://\(viewMoreData.extraDetailsDictionary.carrier_number)")!
            if UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.open(url as URL, options: [ : ], completionHandler: nil)
            }else{
                let alert = AppTheme.showAlert("Number not available." , message:"" , cancelButtonTitle: "OK")
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let alert = AppTheme.showAlert("Number not available." , message:"" , cancelButtonTitle: "OK")
            self.present(alert, animated: true, completion: nil)
        }

        
        
//        let url:NSURL = NSURL(string: "tel://\(viewMoreData.extraDetailsDictionary.carrier_number)")!
//        if UIApplication.shared.canOpenURL(url as URL) {
//            UIApplication.shared.open(url as URL, options: [ : ], completionHandler: nil)
//        }
    }
   
    @IBAction func cancelPopUpAction (_ sender : UIButton) {
        switch sender.tag {
        case 10:
            animateOut(popUp: viewImage)
        case 11:
            animateOut(popUp: viewComment)
        default:
            break
        }
        
    }
    @IBAction func commentButtonAction (_ sender : UIButton) {
        animateIn(popUp: viewComment)
    }
    @IBAction func sendCommentAction (_ sender : UIButton) {
        
        var strError = String()
        if commentTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
            strError = ErrorMesssage.messageEmpty
        }
        //        else  if tfPwd.text!.count < 6 {
        //            strError = ErrorMesssage.passwordLength
        //        }
        
        if strError.isEmpty {
            let data = ["comment": commentTextField.text!,
                        "order_id":"\(self.viewMoreData.extraDetailsDictionary.order_id)"]
            
            callServiceTosendComment(param: data as [String : AnyObject])
        }else{
            let alert = AppTheme.showAlert(strError, message:"" , cancelButtonTitle: "OK")
            self.present(alert, animated: true, completion: nil)
        }
    }
}
// MARK: - UITableView DataSource and Delegate
extension ViewMoreVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case orderStatusTableView:
            return orderStatusDataArray.count == 0 ? 0 : orderStatusDataArray.count+1
        case attachementsTableView:
            return  attachmentDataArray.count == 0 ? 0:attachmentDataArray.count
        case commentTableView:
            return  orderCommentDataArray.count == 0 ? 0:orderCommentDataArray.count
        case assetTblVw:
            return currentAssetList.count == 0 ? 0 : currentAssetList.count
        default:
            return 0
        }
    }
    
    func presentAssetView(assetList: [Asset] ){
        
        currentAssetList = assetList
        
        animateIn(popUp: assetView)
        assetTblVw.dataSource = self
        assetTblVw.delegate = self
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
            
        case assetTblVw:
            let cell = tableView.dequeueReusableCell(withIdentifier: "asset_cell", for: indexPath) as! AssectTableViewCell
            cell.assetIdLbl.text = currentAssetList[indexPath.row].assetId
            cell.assetTypeLbl.text = currentAssetList[indexPath.row].regNum
            return cell
            
        case orderStatusTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.viewMoreOrderStatus, for: indexPath) as! ViewMoreTableViewCell
            
            if indexPath.row == index{
                
                if recentETA == "Can't be calculate."{
                    cell.etaLbl.backgroundColor = UIColor.clear
                    cell.etaLbl.textColor = UIColor.red
                }else{
                    cell.etaLbl.backgroundColor = #colorLiteral(red: 0.2573837936, green: 0.6856743693, blue: 0.9494813085, alpha: 1)
                    cell.etaLbl.textColor = UIColor.white
                }
                
                cell.etaLbl.text = recentETA
                
            }else{
                cell.etaLbl.text = ""
            }
            
            if indexArray.contains(indexPath.row){
                return cell
            }
            cell.nameLbl.isHidden = true
            cell.btnAssets.isHidden = true
            if indexPath.row == 0 {
                if viewMoreData.extraDetailsDictionary.status == JobStatus.inProgress || viewMoreData.extraDetailsDictionary.status == JobStatus.completed {
                    cell.markAsCompleteButton.isHidden = true
                    cell.rightTickImage.isHidden = false
                    cell.ETAImage.isHidden = true
                    cell.processIcon.image = #imageLiteral(resourceName: "completed-progressbar")
                    cell.ActualDateTimeLabel.text = formatter.date(from:enrouteDate)?.formatted(format: "dd/MM/yyyy hh:mm a")
                }else {
                    cell.markAsCompleteButton.isHidden = false
                    cell.markAsCompleteButton.setTitle("En-route", for: .normal)
                    cell.markAsCompleteButton.tag = 10
                    isButtonVisible = true
                    cell.ETAImage.isHidden = false
                    cell.rightTickImage.isHidden = true
                    cell.ActualDateTimeLabel.text = formatter.date(from:orderStatusDataArray[indexPath.row].pickup_date_time_to)?.formatted(format: "dd/MM/yyyy hh:mm a")
                }
                cell.viewPodButton.isHidden = true
                cell.addressLabel.isHidden = true
                cell.quantityLabel.isHidden = true
                cell.titleLabel.text = "En-route to pickUp"
            }else {
                cell.addressLabel.isHidden = false
                cell.quantityLabel.isHidden = false
                let data = orderStatusDataArray[indexPath.row-1]
                
                cell.addressLabel.text = data.address
                cell.quantityLabel.text = "QTY: \(data.assigned_quantity)"
                cell.nameLbl.isHidden = false
                
                if data.data_of == OrderType.pickUp {
                    if data.assetList.isEmpty{
                        cell.btnAssets.isHidden = true
                    }else{
                        cell.btnAssets.isHidden = false
                        cell.btnAssets.setTitle("Assets: \(data.assetList.count)", for: .normal)
                        
                        cell.assetView = {
                            self.presentAssetView( assetList: data.assetList)
                        }
                    }
                    cell.titleLabel.text = "Pick-Up"
                    cell.viewPodButton.isHidden = true
                    cell.nameLbl.text = "Sender: " + data.senderName
                    if data.actual_pickup_date_time.isEmpty {
                        if isButtonVisible {
                            cell.markAsCompleteButton.isHidden = true
//                            cell.rightTickImage.isHidden = false
                            cell.ETAImage.image = #imageLiteral(resourceName: "eta-gray")
                        }else{
                        
                              isButtonVisible = true
                           
                            cell.markAsCompleteButton.isHidden = false
                            cell.markAsCompleteButton.setTitle("Mark As Complete", for: .normal)
                            cell.markAsCompleteButton.tag = 11
                            cell.ETAImage.image = #imageLiteral(resourceName: "eta-blue")
                        }
//                        cell.markAsCompleteButton.isHidden = false
//                        cell.markAsCompleteButton.setTitle("Mark As Complete", for: .normal)
//                        cell.markAsCompleteButton.tag = 11
//                        cell.ETAImage.isHidden = false
                        
                        
                        print(data.etaPickUp)
                        
                        //cell.etaLbl.text = data.etaPickUp
                        cell.rightTickImage.isHidden = true
                        cell.ActualDateTimeLabel.text = formatter.date(from:data.pickup_date_time_to)?.formatted(format: "dd/MM/yyyy hh:mm a")
                    }else{
                        cell.markAsCompleteButton.isHidden = true
                        cell.rightTickImage.isHidden = false
                        cell.processIcon.image = #imageLiteral(resourceName: "completed-progressbar")
                        
                        
                        
                        cell.ETAImage.isHidden = true
                        cell.ActualDateTimeLabel.text = (formatter.date(from:data.pickup_date_time_to)?.formatted(format: "dd/MM/yyyy hh:mm a"))! + "\n ON: "
                        + (formatter.date(from:data.actual_pickup_date_time)?.formatted(format: "dd/MM/yyyy hh:mm a"))!
                        
                    }
                }
                else if data.data_of == OrderType.delivery{
                    cell.titleLabel.text = "Delivery"
                    
                    
                    
                    if data.assetList.isEmpty{
                        cell.btnAssets.isHidden = true
                    }else{
                        cell.btnAssets.isHidden = false
                        cell.btnAssets.setTitle("Assets: \(data.assetList.count)", for: .normal)
                        cell.assetView = {
                            self.presentAssetView( assetList: data.assetList)
                        }
                    }
                    
                    cell.nameLbl.text = "Receiver: " + data.senderName
                    if data.actual_delivery_date_time.isEmpty {
                        if isButtonVisible {
                            cell.markAsCompleteButton.isHidden = true
//                            cell.rightTickImage.isHidden = false
                            cell.ETAImage.image = #imageLiteral(resourceName: "eta-gray")
                        }else{
                               isButtonVisible = true
                           // cell.etaLbl.text = data.etaDelivery
                            cell.markAsCompleteButton.isHidden = false
                            cell.markAsCompleteButton.setTitle("Capture POD", for: .normal)
                            cell.markAsCompleteButton.tag = 12
                            cell.ETAImage.image = #imageLiteral(resourceName: "eta-blue")
                        }
                        //                        cell.markAsCompleteButton.isHidden = false
                        //                        cell.markAsCompleteButton.setTitle("Capture POD", for: .normal)
                        //                        cell.markAsCompleteButton.tag = 12
                        //                        cell.ETAImage.isHidden = false
                        cell.viewPodButton.isHidden = true
                        cell.rightTickImage.isHidden = true
                        cell.ActualDateTimeLabel.text = formatter.date(from:data.delivery_date_time_to)?.formatted(format: "dd/MM/yyyy hh:mm a")
                    }else{
                        cell.markAsCompleteButton.isHidden = true
                        cell.rightTickImage.isHidden = false
                        cell.processIcon.image = #imageLiteral(resourceName: "completed-progressbar")
                       // cell.etaLbl.text = data.etaDelivery
                        cell.viewPodButton.isHidden = false
                        cell.ETAImage.isHidden = true
                        cell.ActualDateTimeLabel.text = (formatter.date(from:data.delivery_date_time_to)?.formatted(format: "dd/MM/yyyy hh:mm a"))! + "\n ON: "
                            + (formatter.date(from:data.actual_delivery_date_time)?.formatted(format: "dd/MM/yyyy hh:mm a"))!
                            //formatter.date(from:data.actual_delivery_date_time)?.formatted(format: "dd/MM/yyyy hh:mm a")!
                    }
                }
                
            }
            cell.ShowPODButtonClick = {
                
                self.ImagePod.kf.indicatorType = .activity
                self.ImagePod.kf.setImage(with: URL(string: self.orderStatusDataArray[indexPath.row-1].pod_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)! ,placeholder: nil )
                self.animateIn(popUp: self.viewImage)
            }
           
            cell.EnRounteButtonClick = {
//                "order_id":1043,
//                "carr_id":70,
//                "en_route_time":"2019-04-26T13:04:54Z",
//                "final_delivery_date_time":"2019-05-30T13:04:54Z"
                let param = [
                    "order_id": self.viewMoreData.extraDetailsDictionary.order_id,
                    "carr_id":SharedPreference.getUserData().carr_id,
                    "en_route_time": Date().formatted(format: "yyyy-MM-dd'T'HH:mm:ss'Z'"),
                    "final_delivery_date_time": nil
                    ] as [String : Any?]
                
                print(param)
                
                
                self.callServiceForUpdateOrderStatus(status: "en_route", paramData: param as [String : Any])
            }
            cell.CapturePODButtonClick = {
                self.index_path = indexPath.row
                self.openCamera ()
            }
            cell.MarkAsCompleteButtonClick = {
//                {
//                    "update_data":{
//                        "pick_id":"726",
//                        "actual_pickup_date_time":"2019-04-27T06:15:11Z"
//
//                    },
//                    "order_id":"821",
//                    "carr_id":"53",
//                    "driver_name":"sandeep2",
//                    "order_job":"ASW202"
//                }
                let userData = SharedPreference.getUserData()
                let param = [
                    "update_data": [
                        "pick_id":"\(self.orderStatusDataArray[indexPath.row-1].id)",
                        "actual_pickup_date_time":Date().formatted(format: "yyyy-MM-dd'T'HH:mm:ss'Z'")
                    ],
                    "order_id": "\(self.viewMoreData.extraDetailsDictionary.order_id)",
                    "carr_id":"\(userData.carr_id)",
                    "driver_name": userData.name,
                    "order_job": "\(self.viewMoreData.extraDetailsDictionary.job_no)",
                    "pod_img": ""
                    ] as [String : Any]
                
                print(userData.carr_com_id)
                print(userData.carr_id)
                print(userData.cate_id)
                print(userData.categ_name)
                
                
                print(param)
                
                self.callServiceForUpdatePickupDropActualTime(serviceType: CommunicationManager.WebServiceType.UpdatePickupActualTime, paramData: param)
            }
            indexArray.append(indexPath.row)
             print("count------->", indexPath.row)
            return cell
        case attachementsTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.attachmentCell, for: indexPath) as! ViewMoreTableViewCell
            let data = self.attachmentDataArray[indexPath.row]
            cell.attachmentNameLabel.text = data.attachment_name

            cell.openFile = { path in
                print(path)
                
                self.showFileWithPath(path: path)
            }
            
               cell.urlString = data.physical_path
//            }
            return cell
            
        case commentTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.commentCell, for: indexPath) as! ViewMoreTableViewCell
            let data = self.orderCommentDataArray[indexPath.row]
            if !data.Created_by_DriverID.name.isEmpty{
                cell.userNameLabel.text = "[" + formatDate(dateStr: data.updated_DT) + "] "  + data.Created_by_DriverID.name + " :"
            }else if !data.Created_by_CustomerID.name.isEmpty{
                cell.userNameLabel.text = "[" + formatDate(dateStr: data.updated_DT) + "] "  + data.Created_by_CustomerID.name + " :"
            }else if !data.Created_by_CarrierID.name.isEmpty{
                cell.userNameLabel.text = "[" + formatDate(dateStr: data.updated_DT) + "] " + data.Created_by_CarrierID.name + " :"
            }
            cell.commentNameLabel.text = data.comment
            return cell
        default:
            return UITableViewCell()
        }
    
        
    }
    
    func showFileWithPath(path: String)
    {
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true
        {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //   return menuItem[indexPath.section].collapsed! ? 0 : 44.0
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        switch tableView {
        case orderStatusTableView:
            orderStatusViewHeight.constant = tableView.contentSize.height
        case attachementsTableView:
            attachementsTableViewHeight.constant = tableView.contentSize.height
        case commentTableView:
            
            print("io")
            
//            if commentTableViewHeight.constant < 400 {
//                commentTableViewHeight.constant = tableView.contentSize.height
//            }else{
//                commentTableViewHeight.constant = 400
//            }
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TravellerHomeVC") as! HomeVC
        //                self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}
// MARK: - UIImagePickerControllerDelegate
extension ViewMoreVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var imageToBase64String: String?
        if let editedImage = info[.editedImage] as? UIImage {
            
           let imgData = editedImage.jpegData(compressionQuality: 0.5)
             imageToBase64String = imgData?.base64EncodedString()
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            
           let imgData = originalImage.jpegData(compressionQuality: 0.5)
            imageToBase64String = imgData?.base64EncodedString()
            picker.dismiss(animated: true, completion: nil)
        }
        

        
            let userData = SharedPreference.getUserData()
            let param = [
                "update_data": [
                    "drop_id":"\(self.orderStatusDataArray[index_path-1].id)",
                    "actual_delivery_date_time":Date().formatted(format: "yyyy-MM-dd'T'HH:mm:ss'Z'")
                ],
                "order_id": "\(self.viewMoreData.extraDetailsDictionary.order_id)",
                "carr_id":"\(userData.carr_id)",
                "driver_name": userData.name,
                "order_job": "\(self.viewMoreData.extraDetailsDictionary.job_no)",
                "pod_img": imageToBase64String!
                ] as [String : Any]
        
        
        
        
            self.callServiceForUpdatePickupDropActualTime(serviceType: CommunicationManager.WebServiceType.UpdateDropActualTime, paramData: param)
            
//        }
    }

    func formatDate(dateStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z" // This formate is input formated .
        
        let formateDate = dateFormatter.date(from:dateStr)!
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm a"
        
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        
        return dateFormatter.string(from: formateDate)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: { })
    }
    
    func etaCalculator() {
        
        var location = PickUpAndDelivery()
        var shouldCalculate = false
        
        for item in self.orderStatusDataArray{
            
            print(orderStatusDataArray.index(of: item)!)
            
            if item.data_of == OrderType.pickUp{
                if item.actual_pickup_date_time == ""{
                    shouldCalculate = true
                    location = item
                    index = orderStatusDataArray.index(of: item)! + 1
                    break
                }
            }else{
                if item.actual_delivery_date_time == ""{
                    location = item
                    shouldCalculate = true
                    index = orderStatusDataArray.index(of: item)! + 1
                    break
                }
            }
        }
        
        print(index)
        
        
        if !shouldCalculate{
            return
        }
        
        function_GetCurrentLocation { (lat, long) in
            
                let wayPointStr = "waypoint0=geo!\(lat),\(long)&waypoint1=geo!\(location.latitude),\(location.longitude)&"
                self.getDropETA(wayPointStr: wayPointStr, model: self.orderStatusDataArray)
        }
//        for index in 1...orderStatusDataArray.count - 1{
//            let wayPointStr = "waypoint0=geo!\(orderStatusDataArray[index - 1].latitude),\(orderStatusDataArray[index - 1].latitude)&waypoint1=geo!\(self.orderStatusDataArray[index].latitude),\(self.orderStatusDataArray[index].longitude)&"
//            self.getDropETA(wayPointStr: wayPointStr, model: self.orderStatusDataArray)
//        }
    }
    
    func getDropETA(wayPointStr: String, model: [PickUpAndDelivery]){
        if AppTheme.isConnectedToNetwork() {
            var serviceStr = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A&\(wayPointStr)mode=fastest;truck;traffic:disabled"
            serviceStr = serviceStr.replacingOccurrences(of: " ", with: "%20")
            
            let request = NSMutableURLRequest(url: URL(string: serviceStr)!)
            request.httpMethod = "GET"
            CommunicationManager().callWebservice(request as URLRequest?) { (responseData, responseCode) in
                DispatchQueue.main.async(execute: {
                    
                    if responseData is NSDictionary {
                        if responseCode == "200" {
                            let response = ((responseData["response"] as! [String: Any])["route"] as! [Any]).first
                            let totalTime = (((response as! [String: Any])["summary"] as! [String: Any])["trafficTime"]) as? Double ?? 00
                            
                            let days = Int(totalTime / (24*3600))
                            let hour = ((totalTime.truncatingRemainder(dividingBy:(24*3600)))/3600)
                            let min = Int((totalTime.truncatingRemainder(dividingBy:(3600*24))).truncatingRemainder(dividingBy: 3600)/60)
                            let time = " \(days)D \(Int(hour))H \(min)M "
                            
                        self.recentETA = time
                            
                            // completion("200", responseData as AnyObject)
                        } else if responseCode == "400" || responseCode == "0" {
                            //Handel Over here long distance
                            print(responseData)
                            
                            self.recentETA = "Can't be calculate."
                            
                            //completion("400", responseData as AnyObject)
                        }
                    }else {
                        // completion("400", ["message":"Netwrok Connection Lost or Slow"] as AnyObject)
                    }
                })
            }
        }else {
            //completion("400", ["message":"Internet connection appears to be offline"] as AnyObject)
        }
    }
    
    
    
    
}
extension ViewMoreVC: CLLocationManagerDelegate{
    
    
    func function_GetCurrentLocation(location:@escaping (String,String)->Void) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            self.completion =  { (lat, lng) in
                location(lat,lng)
            }
        }
    }
    
    
    //Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let isLocation = locations.first {
            completion("\(isLocation.coordinate.latitude)","\(isLocation.coordinate.longitude)")
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            locationManager = nil
        }
    }
}
extension ViewMoreVC: UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController!) -> UIViewController! {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController!) -> CGRect {
        return self.view.frame
    }
 }
